# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Common definitions for the ringroots modules."""

from __future__ import annotations

import abc
import dataclasses
import enum
import pathlib
import sys
import typing


if typing.TYPE_CHECKING:
    import logging
    from typing import Any, Final

    if sys.version_info >= (3, 11):
        from typing import Self
    else:
        from typing_extensions import Self


@dataclasses.dataclass
class Error(Exception):
    """The base class for ringroots-related errors."""

    def __str__(self) -> str:
        """Provide a human-readable error description."""
        raise NotImplementedError(repr(self))


@dataclasses.dataclass
class ChrootNameError(Error, ValueError):
    """An error that occurred while parsing a chroot environment's name."""

    name: str
    """The name of the chroot environment."""

    def __str__(self) -> str:
        """Provide a human-readable error description."""
        return f"Invalid name for a chroot environment: {self.name!r}"


@dataclasses.dataclass
class InternalError(Error, RuntimeError):
    """Something went really, really wrong."""

    obj: Any
    """The thing that had an unexpected value."""

    def __str__(self) -> str:
        """Provide a human-readable error description."""
        return f"ringroots internal error: unexpected object contents: {self.obj!r}"


class ChrootFlavor(str, enum.Enum):
    """The type of a chroot environment."""

    SCHROOT = "schroot"
    """A `schroot` environment, e.g. Debian or Ubuntu."""

    MOCK = "mock"
    """A `mock` environment, e.g. CentOS, AlmaLinux, Rocky Linux."""

    def __str__(self) -> str:
        """Provide a human-readable representation: the value itself."""
        return self.value


class RepoFlavor(str, enum.Enum):
    """The type of a repository specification."""

    URL = "url"
    """Specify the URL of an Apt repository directly."""

    BACKPORTS = "backports"
    """Use the backports suite for the chroot environment's release."""

    ADD_APT_REPO = "add-apt-repo"
    """Use the `add-apt-repository` tool to add e.g. an Ubuntu PPA."""

    def __str__(self) -> str:
        """Provide a human-readable representation: the value itself."""
        return self.value


class PackageType(str, enum.Enum):
    """The type of packages that may be fetched from a repository."""

    DEB = "deb"
    """Debian/Ubuntu binary packages."""

    DEB_SRC = "deb-src"
    """Debian/Ubuntu source packages."""

    RPM = "rpm"
    """RPM binary packages."""

    RPM_SRC = "rpm-src"
    """RPM source packages."""

    def __str__(self) -> str:
        """Provide a human-readable representation: the value itself."""
        return self.value


@dataclasses.dataclass(frozen=True)
class Keyring:
    """A keyring to be added to a chroot environment."""

    url: str
    """The URL that the OpenPGP keyring may be fetched from."""

    checksums: dict[str, str]
    """The checksum hashes to verify the keyring file."""


_UNSPEC: Final = "<...unspecified...>"


class Distro(str, enum.Enum):
    """The name of a distribution."""

    DEBIAN = "debian"
    """Debian GNU/Linux."""

    UBUNTU = "ubuntu"
    """Ubuntu."""

    CENTOS = "centos"
    """CentOS."""

    def __str__(self) -> str:
        """Provide a human-readable representation: the value itself."""
        return self.value


class MirrorType(str, enum.Enum):
    """The type of a mirror - OS, backports, etc."""

    MAIN = "main"
    """The main set of mirrors for a Linux distribution."""

    BACKPORTS = "backports"
    """The set of backports suite mirrors for a Linux distribution."""

    def __str__(self) -> str:
        """Provide a human-readable representation: the value itself."""
        return self.value


DistroMirrorsDict = dict[MirrorType, str]
MirrorsDict = dict[Distro, DistroMirrorsDict]


@dataclasses.dataclass(frozen=True)
class ChrootMeta:
    """Metadata about chroots for a given distribution/release pair."""

    @property
    @abc.abstractmethod
    def flavor(self) -> ChrootFlavor:
        """Get the chroot type."""
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def components(self) -> list[str]:
        """Get the default list of components for Debian/Ubuntu distributions."""
        raise NotImplementedError

    @abc.abstractmethod
    def security_mirror_override(self, mirror: str) -> str | None:
        """Whether to add the -security suite in addition to the -updates one."""
        raise NotImplementedError


@dataclasses.dataclass(frozen=True)
class SChrootChrootMeta(ChrootMeta):
    """Schroot-specific chroot definitions."""

    merged_usr: bool
    """Whether a merged /usr environment is to be created."""

    add_updates: bool
    """Whether to add the -updates repository definitions."""

    @property
    def flavor(self) -> ChrootFlavor:
        """Get the chroot type."""
        return ChrootFlavor.SCHROOT


@dataclasses.dataclass(frozen=True)
class DebianChrootMeta(SChrootChrootMeta):
    """Debian-specific chroot definitions."""

    add_security: bool
    """Whether to add the -security repository definitions."""

    no_firmware: bool
    """Whether to skip the non-free-firmware component."""

    @property
    def components(self) -> list[str]:
        """It contains multitudes."""
        return ["main", "contrib", "non-free"] + ([] if self.no_firmware else ["non-free-firmware"])

    def security_mirror_override(self, mirror: str) -> str | None:  # noqa: ARG002
        """Debian has separate security repos for everything but unstable."""
        if not self.add_security:
            return None

        return "http://security.debian.org/debian-security"


@dataclasses.dataclass(frozen=True)
class UbuntuChrootMeta(SChrootChrootMeta):
    """Ubuntu-specific chroot definitions."""

    @property
    def components(self) -> list[str]:
        """It contains other multitudes."""
        return ["main", "universe", "restricted", "multiverse"]

    def security_mirror_override(self, mirror: str) -> str | None:
        """Ubuntu keeps the -security suites in the same place."""
        return mirror


@dataclasses.dataclass(frozen=True)
class CentOSChrootMeta(ChrootMeta):
    """CentOS-specific chroot definitions."""

    @property
    def flavor(self) -> ChrootFlavor:
        """Get the chroot type."""
        return ChrootFlavor.MOCK

    @property
    def components(self) -> list[str]:
        """Put something here."""
        return ["base"]

    def security_mirror_override(self, mirror: str) -> str | None:
        """Put something here."""
        raise NotImplementedError(repr(mirror))


CHROOT_DEFS: Final = {
    (Distro.UBUNTU, "oracular"): UbuntuChrootMeta(merged_usr=True, add_updates=True),
    (Distro.UBUNTU, "noble"): UbuntuChrootMeta(merged_usr=True, add_updates=True),
    (Distro.UBUNTU, "mantic"): UbuntuChrootMeta(merged_usr=True, add_updates=True),
    (Distro.UBUNTU, "jammy"): UbuntuChrootMeta(merged_usr=True, add_updates=True),
    (Distro.UBUNTU, "focal"): UbuntuChrootMeta(merged_usr=True, add_updates=True),
    (Distro.UBUNTU, "bionic"): UbuntuChrootMeta(merged_usr=False, add_updates=True),
    (Distro.DEBIAN, "unstable"): DebianChrootMeta(
        merged_usr=True, add_updates=False, add_security=False, no_firmware=False
    ),
    (Distro.DEBIAN, "bookworm"): DebianChrootMeta(
        merged_usr=True, add_updates=True, add_security=True, no_firmware=False
    ),
    (Distro.DEBIAN, "bullseye"): DebianChrootMeta(
        merged_usr=False, add_updates=True, add_security=True, no_firmware=False
    ),
    (Distro.CENTOS, "9"): CentOSChrootMeta(),
    (Distro.CENTOS, "8"): CentOSChrootMeta(),
    (Distro.CENTOS, "7"): CentOSChrootMeta(),
}
"""The recognized Debian and Ubuntu releases."""


@dataclasses.dataclass(frozen=True)
class Repo:
    """A single repository to be added to a chroot environment."""

    comment: str = ""
    """A one-line human-readable description of the repository's purpose."""

    flavor: RepoFlavor = RepoFlavor.URL
    """The type of the repository: direct Apt URL, backports, `add-apt-repository`, etc."""

    source: str = dataclasses.field(default=_UNSPEC)
    """The repository source: either a direct URL or a flavor-specific name."""

    suites: list[str] = dataclasses.field(default_factory=lambda: [_UNSPEC])
    """The suites to fetch packages from."""

    types: list[PackageType] = dataclasses.field(default_factory=list)
    """The package types (binary, source) that Apt should install from this repository."""

    components: list[str] = dataclasses.field(default_factory=lambda: [_UNSPEC])
    """The set of components that Apt should look for in each suite."""

    architectures: list[str] = dataclasses.field(default_factory=lambda: [_UNSPEC])
    """The list of architectures that this repository contains packages for."""

    keyring: str | None = None
    """The keyring to validate the repository signatures, if not a standard one."""

    packages: list[str] = dataclasses.field(default_factory=list)
    """The packages to install from this repository, if any."""

    def fill_in_from_chroot(self, name: str, chroot: Chroot, mirrors: MirrorsDict) -> Repo:
        """Check for default values, initialize them."""
        raise NotImplementedError(repr((self, name, chroot, mirrors)))


@dataclasses.dataclass(frozen=True)
class AptRepo(Repo):
    """An Apt repository."""

    def _fill_in_defaults(self, name: str, chroot: Chroot) -> Repo:
        """Fill some of the standard stuff in."""
        comment: Final = (
            self.comment if self.comment else f"Autogenerated repository definition for {name}"
        )
        components: Final = (
            self.components if self.components != [_UNSPEC] else chroot.get_meta().components
        )
        architectures: Final = (
            self.architectures if self.architectures != [_UNSPEC] else [chroot.get_arch(), "all"]
        )
        types: Final = self.types if self.types else [PackageType.DEB, PackageType.DEB_SRC]
        return dataclasses.replace(
            self, comment=comment, types=types, components=components, architectures=architectures
        )

    def _fill_in_from_url(self, name: str, full_name: str, chroot: Chroot) -> Repo:
        """Validate and fill in an URL-based repository."""
        if self.source == _UNSPEC:
            sys.exit(f"No 'source' in the '{full_name}' repo")

        if self.suites == [_UNSPEC]:
            sys.exit(f"No 'suites' in the '{full_name}' repo")

        return self._fill_in_defaults(name, chroot)

    def _fill_in_from_backports(
        self, name: str, full_name: str, chroot: Chroot, mirrors: MirrorsDict
    ) -> Repo:
        """Validate and fill in a backports repository."""

        def build_suites() -> list[str]:
            """Build the list of suites to work with."""
            if self.suites != [_UNSPEC]:
                return self.suites

            # So, yeah, we don't need to add a separate flag, do we?
            chroot_meta: Final = CHROOT_DEFS[(chroot.distro, chroot.release)]
            if not isinstance(chroot_meta, SChrootChrootMeta):
                sys.exit(f"No backports definitions for {chroot.distro}")
            if not chroot_meta.add_updates:
                sys.exit(
                    f"No default suite for the '{chroot.release}' release for "
                    f"the '{full_name}' repo"
                )
            return [f"{chroot.release}-backports"]

        source: Final = (
            self.source if self.source != _UNSPEC else mirrors[chroot.distro][MirrorType.BACKPORTS]
        )
        suites: Final = build_suites()
        return dataclasses.replace(
            self._fill_in_defaults(name, chroot), source=source, suites=suites
        )

    def _fill_in_from_add_apt_repo(self, full_name: str) -> Repo:
        """Validate and fill in an `add-apt-repository` repository."""
        if self.source == _UNSPEC:
            sys.exit(f"No 'source' in the '{full_name}' repo")

        if self.suites != [_UNSPEC] or self.architectures != [_UNSPEC]:
            sys.exit(
                f"Neither 'suites' nor 'architectures' may be specified for "
                f"the '{full_name}' repo"
            )

        types: Final = self.types if self.types else [PackageType.DEB, PackageType.DEB_SRC]
        if PackageType.DEB not in types:
            sys.exit(f"No 'deb' package type for the '{full_name}' repo")

        return dataclasses.replace(self, types=types)

    def fill_in_from_chroot(self, name: str, chroot: Chroot, mirrors: MirrorsDict) -> Repo:
        """Check for default values, initialize them."""
        full_name: Final = f"{chroot.name}.{name}"

        match self.flavor:
            case RepoFlavor.URL:
                return self._fill_in_from_url(name, full_name, chroot)

            case RepoFlavor.BACKPORTS:
                return self._fill_in_from_backports(name, full_name, chroot, mirrors)

            case RepoFlavor.ADD_APT_REPO:
                return self._fill_in_from_add_apt_repo(full_name)

            case _:
                raise NotImplementedError(self.flavor)


@dataclasses.dataclass(frozen=True)
class DNFRepo(Repo):
    """A DNF/Yum repository."""

    def _fill_in_from_url(self, name: str, full_name: str, chroot: Chroot) -> Repo:
        """Validate and fill in an URL-based repository."""
        if self.source == _UNSPEC:
            sys.exit(f"No 'source' in the '{full_name}' repo")

        if self.suites != [_UNSPEC]:
            raise NotImplementedError(repr(self.suites))

        comment: Final = (
            self.comment if self.comment else f"Autogenerated repository definition for {name}"
        )
        types: Final = self.types if self.types else [PackageType.RPM, PackageType.RPM_SRC]
        components: Final = (
            self.components if self.components != [_UNSPEC] else chroot.get_meta().components
        )
        architectures: Final = (
            self.architectures if self.architectures != [_UNSPEC] else [chroot.get_arch()]
        )
        return dataclasses.replace(
            self, comment=comment, types=types, components=components, architectures=architectures
        )

    def fill_in_from_chroot(self, name: str, chroot: Chroot, _mirrors: MirrorsDict) -> Repo:
        """Check for default values, initialize them."""
        full_name: Final = f"{chroot.name}.{name}"

        match self.flavor:
            case RepoFlavor.URL:
                return self._fill_in_from_url(name, full_name, chroot)

            case _:
                raise NotImplementedError(self.flavor)


@dataclasses.dataclass(frozen=True)
class Chroot:
    """A single chroot environment."""

    name: str
    """The name of the chroot environment."""

    distro: Distro
    """The Linux distribution to install in the environment."""

    release: str
    """The short name of the release of the Linux distribution to install."""

    repos: dict[str, Repo] = dataclasses.field(default_factory=dict)
    """The repositories to add and optionally install packages from."""

    def split_name(self) -> tuple[str, str]:
        """Split the instance name into a base name and an architecture name."""
        match self.name.rsplit("-", maxsplit=1):
            case [base, arch]:
                return base, arch

            case _:
                raise ChrootNameError(self.name)

    def get_meta(self) -> ChrootMeta:
        """Get the meta definitions for the chroot type."""
        try:
            return CHROOT_DEFS[(self.distro, self.release)]
        except KeyError:
            sys.exit(f"Unsupported distro/release combination {self.distro}/{self.release}")

    def get_arch(self) -> str:
        """Get the architecture name from the instance name."""
        return self.split_name()[1]

    def get_def_path(self) -> pathlib.Path:
        """Get the name of the schroot definition file."""
        needle: Final = f"[{self.name}]"
        for deffile in pathlib.Path("/etc/schroot/chroot.d").iterdir():
            if deffile.name.startswith(".") or not deffile.is_file():
                continue

            if needle in deffile.read_text(encoding="UTF-8").splitlines():
                return deffile

        return pathlib.Path("/nonexistent")

    def get_chroot_path(self) -> pathlib.Path:
        """Get the path to the chroot's root directory."""
        return pathlib.Path("/var/lib/schroot/chroots") / self.name

    def create_repo(self, repo: Repo) -> Repo:
        """Create an Apt, DNF, etc. repo depending on the chroot environment's flavor."""
        match self.get_meta().flavor:
            case ChrootFlavor.SCHROOT:
                return AptRepo(**dataclasses.asdict(repo))

            case ChrootFlavor.MOCK:
                return DNFRepo(**dataclasses.asdict(repo))

            case other:
                raise NotImplementedError(repr((other, repo)))

    def fill_in_repos(self, repos: dict[str, Repo], mirrors: MirrorsDict) -> Chroot:
        """Return a new instance with all the repositories' unspecified values filled in."""
        return dataclasses.replace(
            self,
            repos={
                name: self.create_repo(repo).fill_in_from_chroot(name, self, mirrors)
                for name, repo in repos.items()
            },
        )


@dataclasses.dataclass(frozen=True)
class Config:
    """Runtime configuration for the chroot management tool."""

    chroots: list[Chroot]
    """The list of chroot environments to create."""

    chroots_by_name: dict[str, Chroot]
    """The chroot environments indexed by name for easier access."""

    log: logging.Logger
    """The logger to send diagnostic and informational messages to."""

    keyrings: dict[str, Keyring]
    """The additional OpenPGP keyrings to install in some chroot environments."""

    mirrors: MirrorsDict
    """The set of mirror URLs to override the default ones."""

    config_file: pathlib.Path
    """The path to the ringroots configuration file."""

    sudo: str
    """The command to use to gain root privileges as needed."""

    utf8_env: dict[str, str]
    """Environment settings to ensure UTF-8 output for child processes."""

    verbose: bool
    """Verbose operation; display diagnostic output."""


@dataclasses.dataclass(frozen=True)
class FetchedKeyring:
    """A keyring that was fetched to a temporary file on disk."""

    keyring: Keyring
    """The OpenPGP keyring definition."""

    path: pathlib.Path
    """The path the OpenPGP keyring file was downloaded to."""


@dataclasses.dataclass
class Cache:
    """Runtime cache for parsed and created objects and files."""

    tempd: pathlib.Path
    """The temporary directory to use as a base of the cache tree."""

    keyring_defs: dict[str, Keyring]
    """The OpenPGP keyrings to be cached."""

    keyrings: dict[str, FetchedKeyring]
    """The OpenPGP keyrings that were downloaded into the cache."""


@dataclasses.dataclass(frozen=True)
class ChEnv(abc.ABC):
    """A generic base class for a chroot environment implementation."""

    name: str
    """The name of the chroot environment."""

    directory: pathlib.Path
    """The path to the chroot environment's root directory."""

    @staticmethod
    @abc.abstractmethod
    def _blurb() -> str:
        """Provide a short descriptive string."""
        raise NotImplementedError

    @classmethod
    @abc.abstractmethod
    def list_all(cls, cfg: Config) -> dict[str, Self]:
        """List all the existing chroot environments of the given type."""
        raise NotImplementedError

    @classmethod
    @abc.abstractmethod
    def create(
        cls,
        cfg: Config,
        cache: Cache,
        chroot_def: Chroot,
    ) -> Self:
        """Create a single chroot environment."""
        raise NotImplementedError

    @classmethod
    @abc.abstractmethod
    def remove(cls, cfg: Config, chroot_def: Chroot) -> None:
        """Remove the chroot environment and its configuration files."""
        raise NotImplementedError

    @abc.abstractmethod
    def update(self, cfg: Config) -> None:
        """Update the packages in a chroot's source environment."""


VERSION: Final = "0.2.0"
"""The version of the ringroots tool."""


FEATURES: Final = {
    "ringroots": VERSION,
    "apt": "0.1",
    "cfg-file": "0.1",
    "cmd-create": "0.1",
    "cmd-list": "0.1",
    "cmd-remove": "0.1",
    "cmd-update": "0.1",
    "format-min": "0.2",
    "format-max": "0.3",
    "keyring": "0.1",
    "mock": "0.1",
    "mirrors": "0.1",
    "repo": "0.2",
    "schroot": "0.1",
    "schroot-impl-sbuild": "0.1",
}
"""The list of features shown by the `--features` command-line option."""
