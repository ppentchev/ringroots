# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Utility functions for the ringroots modules."""

from __future__ import annotations

import functools
import logging
import os
import pathlib
import posix
import pwd
import sys
import typing


if typing.TYPE_CHECKING:
    from typing import Final


def find_home_dir() -> pathlib.Path:
    """Find the current home directory."""
    env_home: Final = os.environ.get("HOME")
    if env_home is not None:
        return pathlib.Path(env_home)

    return pathlib.Path(pwd.getpwuid(posix.getuid()).pw_dir)


@functools.lru_cache
def build_logger(*, verbose: bool = False) -> logging.Logger:
    """Build a logger that outputs to the standard output and error streams.

    Messages of level `INFO` go to the standard output stream.
    Messages of level `WARNING` and higher go to the standard error stream.
    If `verbose` is true, messages of level `DEBUG` also go to the standard error stream.
    """
    logger: Final = logging.getLogger("ringroots")
    logger.setLevel(logging.DEBUG if verbose else logging.INFO)
    logger.propagate = False

    diag_handler: Final = logging.StreamHandler(sys.stderr)
    diag_handler.setLevel(logging.DEBUG if verbose else logging.WARNING)
    diag_handler.addFilter(lambda rec: rec.levelno != logging.INFO)
    logger.addHandler(diag_handler)

    info_handler: Final = logging.StreamHandler(sys.stdout)
    info_handler.setLevel(logging.INFO)
    info_handler.addFilter(lambda rec: rec.levelno == logging.INFO)
    logger.addHandler(info_handler)

    return logger
