# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Common definitions for the apt-parse library."""

from __future__ import annotations


class ParseError(Exception):
    """An error that occurred during the parsing of apt's files or output."""
