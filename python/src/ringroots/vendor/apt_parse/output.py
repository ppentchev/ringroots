# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Parse the output of some Apt commands."""

from __future__ import annotations

import dataclasses

import pyparsing as pyp

from . import defs


_UNDEFINED = "<undefined>"


@dataclasses.dataclass
class PyParseError(defs.ParseError, AssertionError):
    """The `pyparsing` library passed us an unexpected series of tokens."""

    tokens: pyp.ParseResults
    """The tokens we did not expect to get."""

    def __str__(self) -> str:
        """Provide a human-readable description of the error."""
        return f"Could not parse the `apt policy` output: unexpected tokens: {self.tokens!r}"


@dataclasses.dataclass(frozen=True)
class PolicyRepo:
    """A single repository definition parsed out of the `apt-cache policy` output."""

    url: str
    suite: str
    component: str
    release: dict[str, str] | None
    origin: str | None


@dataclasses.dataclass(frozen=True)
class PinnedPackage:
    """Represent a single package pinned to a specific version or release."""

    name: str
    version: str
    prio: int


@dataclasses.dataclass(frozen=True)
class PolicySection:
    """Base class for representing the top-level sections of the `apt-cache policy` output."""


@dataclasses.dataclass(frozen=True)
class PolicyGlobal(PolicySection):
    """Represent the global policy, all the repositories known to Apt."""

    repos: list[PolicyRepo]


@dataclasses.dataclass(frozen=True)
class PolicyPinned(PolicySection):
    """Represent the packages pinned to specific versions or releases."""

    packages: list[PinnedPackage]


@dataclasses.dataclass(frozen=True)
class RepoAttr:
    """Base class for representing attribute lines for each repository."""


@dataclasses.dataclass(frozen=True)
class RepoAttrRelease(RepoAttr):
    """Represent a "release" line."""

    attrs: dict[str, str]


@dataclasses.dataclass(frozen=True)
class RepoAttrOrigin(RepoAttr):
    """Represent an "origin" line."""

    origin: str


_p_spc = pyp.Word(" \t")

_p_unsigned_int = pyp.Literal("0") | pyp.Word("123456789", "0123456789")

_p_signed_int = pyp.Opt(pyp.Literal("-")) + _p_unsigned_int

_p_filename = pyp.Char("/") + pyp.CharsNotIn(" \t\n")

_p_repo_loc_filename = _p_filename

_p_arch = pyp.Word("abcdefghijklmnopqrstuvwxyz", "abcdefghijklmnopqrstuvwxyz0123456789-")

# WHEE: add an "arch" member to PolicyRepo
_p_repo_loc_full = (
    pyp.CharsNotIn(" \t\n")("url")
    + _p_spc.suppress()
    + pyp.CharsNotIn(" \t\n/")("suite")
    + pyp.Char("/").suppress()
    + pyp.Opt(pyp.CharsNotIn(" \t\n")("component"))
    + pyp.Opt(_p_spc.suppress() + _p_arch).suppress()
    + _p_spc.suppress()
    + pyp.Literal("Packages").suppress()
)

_p_repo_loc = _p_repo_loc_filename | _p_repo_loc_full

# WHEE: add a "prio" field to PolicyRepo
_p_repo_line_start = (
    _p_spc.suppress()
    + _p_signed_int.suppress()
    + _p_spc.suppress()
    + _p_repo_loc
    + pyp.Char("\n").suppress()
)

_p_repo_line_attr_release_attr = (
    pyp.Char("abcdefghijklmnopqrstuvwxyz")("key")
    + pyp.Char("=").suppress()
    + pyp.Opt(pyp.CharsNotIn(",\n")("value"))
)

_p_repo_line_attr_release_attrs = _p_repo_line_attr_release_attr + pyp.ZeroOrMore(
    pyp.Char(",").suppress() + _p_repo_line_attr_release_attr
)

_p_repo_line_attr_release = (
    _p_spc.suppress()
    + pyp.Literal("release").suppress()
    + _p_spc.suppress()
    + _p_repo_line_attr_release_attrs
    + pyp.Char("\n").suppress()
)

_p_repo_line_attr_origin = (
    _p_spc.suppress()
    + pyp.Literal("origin").suppress()
    + _p_spc.suppress()
    + pyp.CharsNotIn("\n")
    + pyp.Char("\n").suppress()
)

_p_repo_line_attr = _p_repo_line_attr_release | _p_repo_line_attr_origin

_p_repo = _p_repo_line_start + pyp.OneOrMore(_p_repo_line_attr)

_p_repos = pyp.OneOrMore(_p_repo)

_p_pinned = (
    _p_spc.suppress()
    + pyp.CharsNotIn(" \t\n")("name")
    + _p_spc.suppress()
    + pyp.Literal("->").suppress()
    + _p_spc.suppress()
    + pyp.CharsNotIn(" \t\n")("version")
    + _p_spc.suppress()
    + pyp.Literal("with priority").suppress()
    + _p_spc.suppress()
    + _p_signed_int("prio")
    + pyp.Char("\n").suppress()
)

_p_policy_global = pyp.Literal("Package files:\n").suppress() + _p_repos

_p_policy_pinned = pyp.Literal("Pinned packages:\n").suppress() + pyp.ZeroOrMore(_p_pinned)

_p_policy = pyp.OneOrMore(_p_policy_global | _p_policy_pinned)


@_p_unsigned_int.set_parse_action
def _parse_unsigned_int(tokens: pyp.ParseResults) -> int:
    """Parse an unsigned integer value (0 or more)."""
    match list(tokens):
        case [value] if isinstance(value, str):
            return int(value)

        case _:
            raise PyParseError(tokens)


@_p_signed_int.set_parse_action
def _parse_signed_int(tokens: pyp.ParseResults) -> int:
    """Parse a signed integer value."""
    match list(tokens):
        case [value] if isinstance(value, int):
            return value

        case [sign, value] if isinstance(sign, str) and isinstance(value, int):
            return -value

        case _:
            raise PyParseError(tokens)


def _do_parse_filename(tokens: pyp.ParseResults) -> str:
    """Parse a whole filename."""
    match list(tokens):
        case [slash, name] if isinstance(slash, str) and isinstance(name, str):
            return slash + name

        case _:
            raise PyParseError(tokens)


@_p_filename.set_parse_action
def _parse_filename(tokens: pyp.ParseResults) -> str:
    """Parse a whole filename."""
    return _do_parse_filename(tokens)


@_p_repo_loc_filename.set_parse_action
def _parse_repo_loc_filename(tokens: pyp.ParseResults) -> PolicyRepo:
    """Parse a repository "location" consisting of a single filename."""
    filename = _do_parse_filename(tokens)
    return PolicyRepo(
        url="file://" + filename, suite=_UNDEFINED, component=_UNDEFINED, release=None, origin=None
    )


@_p_repo_loc_full.set_parse_action
def _parse_repo_loc_full(tokens: pyp.ParseResults) -> PolicyRepo:
    """Build a repo location out of its parts (suite, component, etc)."""
    url = tokens["url"]
    suite = tokens["suite"]
    try:
        component = tokens["component"]
    except KeyError:
        component = ""
    if not isinstance(url, str) or not isinstance(suite, str) or not isinstance(component, str):
        raise PyParseError(tokens)
    return PolicyRepo(
        url=url,
        suite=suite,
        component=component,
        release=None,
        origin=None,
    )


@_p_repo_line_start.set_parse_action
def _parse_repo_line_start(tokens: pyp.ParseResults) -> PolicyRepo:
    """Build up a representation of the header line of a repository."""
    match list(tokens):
        case [repo] if isinstance(repo, PolicyRepo):
            return repo

        case _:
            raise PyParseError(tokens)


@_p_repo_line_attr_release_attr.set_parse_action
def _parse_repo_line_attr_release_attr(tokens: pyp.ParseResults) -> tuple[str, str]:
    """Build up a single release line attribute."""
    key = tokens["key"]
    try:
        value = tokens["value"]
    except KeyError:
        value = ""
    return (key, value)


@_p_repo_line_attr_release_attrs.set_parse_action
def _parse_repo_line_attr_release_attrs(tokens: pyp.ParseResults) -> dict[str, str]:
    """Build up the release line attributes."""
    match list(tokens):
        case [line_attr, *release_attrs] if isinstance(tokens[0], tuple) and all(
            isinstance(item, tuple) for item in release_attrs
        ):
            return dict([line_attr, *release_attrs])

        case _:
            raise PyParseError(tokens)


@_p_repo_line_attr_release.set_parse_action
def _parse_repo_line_attr_release(tokens: pyp.ParseResults) -> RepoAttr:
    """Store the "release" attribute."""
    match list(tokens):
        case [attrs] if isinstance(attrs, dict):
            return RepoAttrRelease(attrs)

        case _:
            raise PyParseError(tokens)


@_p_repo_line_attr_origin.set_parse_action
def _parse_repo_line_attr_origin(tokens: pyp.ParseResults) -> RepoAttr:
    """Store the "origin" attribute."""
    match list(tokens):
        case [origin] if isinstance(origin, str):
            return RepoAttrOrigin(origin)

        case _:
            raise PyParseError(tokens)


@_p_repo.set_parse_action
def _parse_repo(tokens: pyp.ParseResults) -> PolicyRepo:
    """Merge the "release" and "origin" attributes into the `PolicyRepo` object."""
    match list(tokens):
        case [res, *attrs] if isinstance(res, PolicyRepo) and all(
            isinstance(item, RepoAttr) for item in attrs
        ):
            for attr in attrs:
                if isinstance(attr, RepoAttrRelease):
                    if res.release is not None:
                        raise PyParseError(tokens)
                    res = dataclasses.replace(res, release=attr.attrs)
                elif isinstance(attr, RepoAttrOrigin):
                    if res.origin is not None:
                        raise PyParseError(tokens)
                    res = dataclasses.replace(res, origin=attr.origin)
                else:
                    raise PyParseError(tokens)

            return res

        case _:
            raise PyParseError(tokens)


@_p_repos.set_parse_action
def _parse_repos(tokens: pyp.ParseResults) -> list[PolicyRepo]:
    """Wrap up the list of parsed repository definitions."""
    res = list(tokens)
    if not res or any(not isinstance(item, PolicyRepo) for item in res):
        raise PyParseError(tokens)
    return res


@_p_policy_global.set_parse_action
def _parse_policy_global(tokens: pyp.ParseResults) -> PolicyGlobal:
    """Store the repository definitions into a `PolicyGlobal` object."""
    res = list(tokens)
    if not res or any(not isinstance(item, PolicyRepo) for item in res):
        raise PyParseError(tokens)
    return PolicyGlobal(repos=res)


@_p_pinned.set_parse_action
def _parse_pinned(tokens: pyp.ParseResults) -> PinnedPackage:
    """Build a single pinned package definition."""
    match list(tokens):
        case [name, version, prio] if isinstance(name, str) and isinstance(
            version, str
        ) and isinstance(prio, str):
            return PinnedPackage(name, version, int(prio))

        case _:
            raise PyParseError(tokens)


@_p_policy_pinned.set_parse_action
def _parse_policy_pinned(tokens: pyp.ParseResults) -> PolicyPinned:
    """Wrap the list of pinned packages into a `PolicyPinned` object."""
    if any(not isinstance(item, PinnedPackage) for item in tokens):
        raise PyParseError(tokens)
    return PolicyPinned(packages=list(tokens))


_p_policy_complete = _p_policy.leave_whitespace()


def parse_policy_repos(contents: str) -> list[PolicyRepo]:
    """Parse the `apt-cache policy` output."""
    tokens = _p_policy_complete.parse_string(contents, parse_all=True)
    res = list(tokens)
    if any(not isinstance(item, PolicySection) for item in res):
        raise PyParseError(tokens)
    sect_repos = [item for item in res if isinstance(item, PolicyGlobal)]
    if len(sect_repos) != 1:
        raise PyParseError(tokens)
    return sect_repos[0].repos
