# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Definitions for the 0.2 config format."""

from __future__ import annotations

import dataclasses

from ringroots import defs

from . import v03


@dataclasses.dataclass(frozen=True)
class Repo:
    """A single repository to be added to a chroot environment."""

    flavor: defs.RepoFlavor = defs.RepoFlavor.URL
    """The type of the repository: direct Apt URL, backports, `add-apt-repository`, etc."""

    source: str = dataclasses.field(default=defs._UNSPEC)  # noqa: SLF001
    """The repository source: either a direct URL or a flavor-specific name."""

    suites: list[str] = dataclasses.field(default_factory=lambda: [defs._UNSPEC])  # noqa: SLF001
    """The suites to fetch packages from."""

    types: list[defs.PackageType] = dataclasses.field(default_factory=list)
    """The package types (binary, source) that Apt should install from this repository."""

    components: list[str] = dataclasses.field(default_factory=lambda: [defs._UNSPEC])  # noqa: SLF001
    """The set of components that Apt should look for in each suite."""

    architectures: list[str] = dataclasses.field(default_factory=lambda: [defs._UNSPEC])  # noqa: SLF001
    """The list of architectures that this repository contains packages for."""

    keyring: str | None = None
    """The keyring to validate the repository signatures, if not a standard one."""

    packages: list[str] = dataclasses.field(default_factory=list)
    """The packages to install from this repository, if any."""

    def upgrade(self) -> defs.Repo:
        """Upgrade the repo definition to the current format version."""
        return defs.Repo(
            flavor=self.flavor,
            source=self.source,
            suites=self.suites,
            types=self.types,
            components=self.components,
            architectures=self.architectures,
            keyring=self.keyring,
            packages=self.packages,
        )


@dataclasses.dataclass(frozen=True)
class ChrootWithoutName:
    """A single chroot environment."""

    distro: defs.Distro
    """The Linux distribution to install."""

    release: str
    """The suite/release of the Linux distribution to install."""

    repos: dict[str, Repo] = dataclasses.field(default_factory=dict)
    """The repositories to configure and optionally packages to install."""

    def upgrade(self) -> v03.ChrootWithoutName:
        """Upgrade the chroot definition to the current format version."""
        return v03.ChrootWithoutName(
            distro=self.distro,
            release=self.release,
            repos={name: repo.upgrade() for name, repo in self.repos.items()},
        )


@dataclasses.dataclass(frozen=True)
class ConfigTop:
    """The top-level configuration settings for format 0.2."""

    format: v03.ConfigFormat
    """Metadata about this file's configuration format."""

    chroots: dict[str, ChrootWithoutName]
    """The chroot environments to maintain."""

    mirrors: v03.ConfigMirrors = dataclasses.field(default_factory=v03.ConfigMirrors)
    """The override mirrors for distribution repositories."""

    keyrings: dict[str, defs.Keyring] = dataclasses.field(default_factory=dict)
    """Additional OpenPGP keyrings to download and install into the chroot environments."""

    def upgrade(self) -> v03.ConfigTop:
        """Upgrade the configuration to the current format version."""
        return v03.ConfigTop(
            format=self.format,
            chroots={name: chroot.upgrade() for name, chroot in self.chroots.items()},
            mirrors=self.mirrors,
            keyrings=self.keyrings,
        )
