# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Parse the ringroots.toml configuration file."""

from __future__ import annotations

import functools
import sys
import typing

import typedload.dataloader

from ringroots import defs

from . import v02
from . import v03


if sys.version_info >= (3, 11):
    import tomllib
else:
    import tomli as tomllib


if typing.TYPE_CHECKING:
    import pathlib
    from typing import Any, Final


DEF_MIRROR_DEBIAN: Final = "http://127.0.0.1:9999/debian"
"""The default mirror for Debian chroot environments."""

DEF_MIRROR_UBUNTU: Final = "http://127.0.0.1:9999/ubuntu"
"""The default mirror for Ubuntu chroot environments."""


@functools.cache
def get_loader(*, failonextra: bool) -> typedload.dataloader.Loader:
    """Construct a data loader, enabling PEP 563 deferred annotations support."""
    return typedload.dataloader.Loader(pep563=True, failonextra=failonextra)


def load_and_upgrade(
    config_file: pathlib.Path, data: dict[str, Any], ver_major: int, ver_minor: int
) -> v03.ConfigTop:
    """Parse the configuration, upgrade to the latest version if necessary."""
    try:
        match ver_major, ver_minor:
            case (0, minor) if minor < 2:  # noqa: PLR2004
                sys.exit(
                    f"Config format 0.{ver_minor} not supported, please migrate to at least 0.2"
                )

            # OK, this is a bit of a lie; we will allow "comment" in 0.2 files, too
            case 0, 2:
                return get_loader(failonextra=True).load(data, v02.ConfigTop).upgrade()

            case 0, 3:
                return get_loader(failonextra=True).load(data, v03.ConfigTop)

            case 0, _:
                return get_loader(failonextra=False).load(data, v03.ConfigTop)

            case _:
                sys.exit(f"Unsupported format version {ver_major}.{ver_minor} in {config_file}")
    except ValueError as err:
        sys.exit(f"Could not parse {config_file}: {err}")

    sys.exit("Internal error: how did we even get here?")


def determine_mirrors(cfg_mirrors: v03.ConfigMirrors) -> defs.MirrorsDict:
    """Use the default URLs for the mirrors not overridden in the config file."""
    debian_main: Final = cfg_mirrors.debian if cfg_mirrors.debian is not None else DEF_MIRROR_DEBIAN
    ubuntu_main: Final = cfg_mirrors.ubuntu if cfg_mirrors.ubuntu is not None else DEF_MIRROR_UBUNTU

    return {
        defs.Distro.DEBIAN: {
            defs.MirrorType.MAIN: debian_main,
            defs.MirrorType.BACKPORTS: cfg_mirrors.debian_backports
            if cfg_mirrors.debian_backports is not None
            else debian_main,
        },
        defs.Distro.UBUNTU: {
            defs.MirrorType.MAIN: ubuntu_main,
            defs.MirrorType.BACKPORTS: cfg_mirrors.ubuntu_backports
            if cfg_mirrors.ubuntu_backports is not None
            else ubuntu_main,
        },
    }


def parse_config_file_with_tools(
    config_file: pathlib.Path,
) -> tuple[v03.ConfigData, dict[str, dict[str, Any]]]:
    """Parse the ringroots.toml config file, also return the raw structure."""
    try:
        raw: Final = tomllib.load(config_file.open(mode="rb"))
    except OSError as err:
        sys.exit(f"Could not read {config_file}: {err}")
    except ValueError as err:
        sys.exit(f"Could not parse {config_file} as valid TOML: {err}")
    try:
        ver_major: Final[int] = raw["format"]["version"]["major"]
        ver_minor: Final[int] = raw["format"]["version"]["minor"]
    except (TypeError, KeyError) as err:
        sys.exit(f"Could not parse the format.version field of {config_file}: {err}")
    if (
        not isinstance(ver_major, int)
        or not isinstance(ver_minor, int)
        or ver_major < 0
        or ver_minor < 0
    ):
        sys.exit(f"{config_file}: format.version.{{major,minor}} must be non-negative integers")

    # Remove any [tool.*] sections before parsing and validation.
    tools: Final = raw.pop("tool", {})

    data: Final = load_and_upgrade(config_file, raw, ver_major, ver_minor)
    mirrors: Final = determine_mirrors(data.mirrors)
    return (
        v03.ConfigData(
            chroots=[
                defs.Chroot(
                    name=name, distro=raw.distro, release=raw.release, repos={}
                ).fill_in_repos(raw.repos, mirrors)
                for name, raw in data.chroots.items()
            ],
            mirrors=mirrors,
            keyrings=data.keyrings,
        ),
        tools,
    )


def parse_config_file(config_file: pathlib.Path) -> v03.ConfigData:
    """Parse the ringroots.toml config file."""
    return parse_config_file_with_tools(config_file)[0]
