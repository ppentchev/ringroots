# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Definitions for the 0.3 config format."""

from __future__ import annotations

import dataclasses
import typing


if typing.TYPE_CHECKING:
    from ringroots import defs


@dataclasses.dataclass(frozen=True)
class ConfigFormatVersion:
    """The config format version specification."""

    major: int
    """The version major number."""

    minor: int
    """The version minor number."""


@dataclasses.dataclass(frozen=True)
class ConfigFormat:
    """The top-level format specification, mainly the version number."""

    version: ConfigFormatVersion
    """The version of the configuration format."""


@dataclasses.dataclass(frozen=True)
class ConfigTopFormatOnly:
    """Parse the format.version specification."""

    format: ConfigFormat
    """Metadata about the format of this configuration file, mainly the version number."""


@dataclasses.dataclass(frozen=True)
class ChrootWithoutName:
    """A single chroot environment."""

    distro: defs.Distro
    """The Linux distribution to install."""

    release: str
    """The suite/release of the Linux distribution to install."""

    repos: dict[str, defs.Repo] = dataclasses.field(default_factory=dict)
    """The repositories to configure and optionally packages to install."""


@dataclasses.dataclass(frozen=True)
class ConfigMirrors:
    """The mirrors to use, if specified."""

    debian: str | None = dataclasses.field(default=None)
    """Override mirrors for Debian installations."""

    ubuntu: str | None = dataclasses.field(default=None)
    """Override mirrors for Ubuntu installations."""

    debian_backports: str | None = dataclasses.field(default=None)
    """Override mirrors for Debian backports repositories."""

    ubuntu_backports: str | None = dataclasses.field(default=None)
    """Override mirrors for Ubuntu backports repositories."""


@dataclasses.dataclass(frozen=True)
class ConfigTop:
    """The top-level configuration settings for format 0.3."""

    format: ConfigFormat
    """Metadata about this file's configuration format."""

    chroots: dict[str, ChrootWithoutName]
    """The chroot environments to maintain."""

    mirrors: ConfigMirrors = dataclasses.field(default_factory=ConfigMirrors)
    """The override mirrors for distribution repositories."""

    keyrings: dict[str, defs.Keyring] = dataclasses.field(default_factory=dict)
    """Additional OpenPGP keyrings to download and install into the chroot environments."""


@dataclasses.dataclass(frozen=True)
class ConfigData:
    """Configuration settings parsed from the ringroots.toml file."""

    chroots: list[defs.Chroot]
    """The chroot environments to maintain."""

    mirrors: defs.MirrorsDict
    """The override mirrors for distribution repositories."""

    keyrings: dict[str, defs.Keyring]
    """Additional OpenPGP keyrings to download and install into the chroot environments."""
