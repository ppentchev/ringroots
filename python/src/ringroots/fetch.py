# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Fetch various data to local temporary directories."""

from __future__ import annotations

import hashlib
import sys
import typing
import urllib.parse
import urllib.request

from . import defs


if typing.TYPE_CHECKING:
    from typing import Final


def ensure_keyring(cfg: defs.Config, cache: defs.Cache, name: str) -> None:
    """Fetch a keyring if necessary."""
    if name in cache.keyrings:
        return

    cfg.log.debug("Fetching the %(name)s keyring", {"name": name})
    keyring: Final = cache.keyring_defs.get(name)
    if keyring is None:
        sys.exit(f"The {name!r} keyring is referenced, but not defined")

    filename: Final = urllib.parse.urlparse(keyring.url).path.rsplit("/", maxsplit=1)[1]
    if not filename:
        sys.exit(f"No filename in the URL of the {name!r} keyring")
    path: Final = cache.tempd / "keyrings" / filename
    path.parent.mkdir(exist_ok=True)

    hashers: Final = {h_name: hashlib.new(h_name) for h_name in keyring.checksums}

    cfg.log.debug(
        "- fetching %(keyring_url)s to %(path)s", {"keyring_url": keyring.url, "path": path}
    )
    try:
        # We allow any kind of URL here; should we only accept HTTP and HTTPS?
        with (
            path.open(mode="wb") as outf,
            urllib.request.urlopen(  # noqa: S310
                keyring.url
            ) as resp,
        ):
            cfg.log.debug("- writing out the response")
            while True:
                data = resp.read(32768)
                if not data:
                    break
                outf.write(data)
                for hasher in hashers.values():
                    hasher.update(data)
    except OSError as err:
        sys.exit(f"Could not fetch {keyring.url} to {path}: {err}")

    cfg.log.debug("- wrote out %(size)d bytes", {"size": path.stat().st_size})
    cfg.log.debug("- verifying the accumulated checksums")
    for h_name, expected in keyring.checksums.items():
        actual = hashers[h_name].hexdigest()
        if actual != expected:
            sys.exit(
                f"Wrong {h_name} checksum for the {name} keyring: "
                f"expected {expected!r}, got {actual!r}"
            )

    cache.keyrings[name] = defs.FetchedKeyring(keyring=keyring, path=path)
