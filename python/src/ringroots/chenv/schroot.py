# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Handle schroot environments."""

from __future__ import annotations

import configparser
import dataclasses
import itertools
import pathlib
import subprocess  # noqa: S404
import sys
import tempfile
import typing

from ringroots import defs
from ringroots import fetch
from ringroots.vendor.apt_parse import output as apt_output


if typing.TYPE_CHECKING:
    from collections.abc import Callable
    from typing import Final

    if sys.version_info >= (3, 11):
        from typing import Self
    else:
        from typing_extensions import Self


def matches_policy(repo: defs.Repo, p_repo: apt_output.PolicyRepo) -> bool:
    """Determine whether a policy definition matches this repository."""
    if p_repo.suite not in repo.suites or p_repo.component not in repo.components:
        return False

    match repo.flavor:
        case defs.RepoFlavor.URL | defs.RepoFlavor.BACKPORTS:
            return repo.source == p_repo.url

        case _:
            raise NotImplementedError(repr((repo, p_repo)))


@dataclasses.dataclass(frozen=True)
class ChrootSourceUpdateState:
    """A helper class for parsing the schroot definitions file during creation."""

    lines: list[str]
    """The individual lines of the schroot definitions file."""

    changed: int
    """The number of lines that we needed to change."""


@dataclasses.dataclass(frozen=True)
class SChroot(defs.ChEnv):
    """A single existing schroot environment."""

    @classmethod
    def list_all(cls, cfg: defs.Config) -> dict[str, Self]:
        """List all the existing schroot environments."""
        cfg.log.debug("Looking for any existing %(blurb)ss", {"blurb": cls._blurb()})
        defined: Final = [
            chroot.name
            for chroot in cfg.chroots
            if chroot.get_meta().flavor == defs.ChrootFlavor.SCHROOT
        ]
        if not defined:
            return {}

        try:
            data: Final = subprocess.check_output(
                ["schroot", "--config", cls._list_cmdline_option()],
                encoding="UTF-8",
                env=cfg.utf8_env,
            )
        except FileNotFoundError:
            return {}

        parser: Final = configparser.ConfigParser(interpolation=None)
        try:
            parser.read_string(data)
        except ValueError as err:
            sys.exit(f"Could not parse the output of 'schroot --config --all': {err}")

        return {
            name: cls._parse_list_section(cfg, name, parser[name])
            for name in parser.sections()
            if name in defined
        }

    @staticmethod
    def _blurb() -> str:
        """Provide a short descriptive string."""
        return "schroot environment"

    @staticmethod
    def _list_cmdline_option() -> str:
        """Get the schroot command-line option to list these objects."""
        return "--all"

    @classmethod
    def _parse_list_section(
        cls, _cfg: defs.Config, name: str, section: configparser.SectionProxy
    ) -> Self:
        """Parse a single schroot definition."""
        try:
            return cls(
                name=name,
                directory=pathlib.Path(section["directory"]),
            )
        except (KeyError, ValueError) as err:
            sys.exit(f"Could not parse the {name} schroot configuration: {err}")

    def run_command(
        self,
        cfg: defs.Config,
        command: list[str],
        *,
        capture_output: bool = False,
        sid: str | None = None,
    ) -> str:
        """Run a command within the chroot environment."""
        res: Final = subprocess.run(
            ["schroot"]
            + (["-r", "-c", sid] if sid is not None else ["-c", f"source:{self.name}"])
            + [
                "-u",
                "root",
                "-d",
                "/",
                "--",
                "env",
                "LC_ALL=C.UTF-8",
                "LANGUAGE=",
                "DEBIAN_FRONTEND=noninteractive",
            ]
            + command,
            capture_output=capture_output,
            check=True,
            encoding="UTF-8",
            env=cfg.utf8_env,
        )

        if not capture_output:
            return ""

        assert hasattr(res, "stdout")  # noqa: S101  # mypy needs this
        assert isinstance(res.stdout, str)  # noqa: S101  # mypy needs this
        return res.stdout

    def run_apt_update(self, cfg: defs.Config, *, sid: str | None = None) -> None:
        """Run apt-get update within the chroot environment."""
        self.run_command(cfg, ["apt-get", "update"], sid=sid)

    def run_apt_install(
        self, cfg: defs.Config, packages: list[str], *, sid: str | None = None
    ) -> None:
        """Install some packages within the chroot environment."""
        cfg.log.debug("Installing packages: %(packages)s", {"packages": " ".join(packages)})
        self.run_command(cfg, ["apt-get", "-y", "install", "--", *packages], sid=sid)

    def run_apt_upgrade(self, cfg: defs.Config, *, sid: str | None = None) -> None:
        """Run apt-get dist-upgrade within the chroot environment."""
        self.run_command(cfg, ["apt-get", "-y", "dist-upgrade"], sid=sid)

    def run_apt_clean(self, cfg: defs.Config, *, sid: str | None = None) -> None:
        """Run apt-get dist-clean within the chroot environment."""
        self.run_command(cfg, ["apt-get", "clean"], sid=sid)

    def copy_file(self, cfg: defs.Config, source: pathlib.Path, destination: pathlib.Path) -> None:
        """Install a file into the chroot environment."""
        subprocess.check_call(
            [
                cfg.sudo,
                "install",
                "-o",
                "root",
                "-g",
                "root",
                "-m",
                "644",
                "--",
                source,
                self.directory / destination,
            ],
            env=cfg.utf8_env,
        )

    def create_repo_file(  # noqa: PLR0913
        self,
        cfg: defs.Config,
        cache: defs.Cache,
        repo_base: pathlib.Path,
        name: str,
        repo: defs.Repo,
    ) -> None:
        """Create an /etc/apt/sources.list.d/*.sources file."""
        repo_file: Final = repo_base / f"{name}.sources"
        cfg.log.debug("Creating the %(filename)s temporary file", {"filename": repo_file.name})
        signed: Final = (
            f"Signed-By: /usr/share/keyrings/{cache.keyrings[repo.keyring].path.name}\n"
            if repo.keyring is not None
            else ""
        )
        repo_file.write_text(
            f"""# {repo.comment}
Types: {' '.join(rtype.value for rtype in repo.types)}
URIs: {repo.source}
Suites: {' '.join(repo.suites)}
Components: {' '.join(repo.components)}
Architectures: {' '.join(repo.architectures)}
{signed}""",
            encoding="UTF-8",
        )
        cfg.log.debug("Installing %(filename)s", {"filename": repo_file.name})
        self.copy_file(cfg, repo_file, pathlib.Path("etc/apt/sources.list.d") / repo_file.name)

    def add_apt_repo(self, cfg: defs.Config, repo: defs.Repo, *, first: bool) -> None:
        """Run add-apt-repository with the specified repo source."""
        if first:
            self.run_apt_install(cfg, ["software-properties-common"])

        cfg.log.debug("Running `add-apt-repository %(source)s`", {"source": repo.source})
        self.run_command(
            cfg,
            ["add-apt-repository"]
            + (["--enable-source"] if defs.PackageType.DEB_SRC in repo.types else [])
            + ["--yes", "--no-update", "--", repo.source],
        )

    def setup_repos(self, cfg: defs.Config, cache: defs.Cache, chroot_def: defs.Chroot) -> None:
        """Add additional Apt repository definitions."""
        if not chroot_def.repos:
            cfg.log.debug("No repositories to add to the %(name)s environment", {"name": self.name})
            return

        self.run_apt_install(cfg, ["ca-certificates"])

        for name in (
            repo.keyring for repo in chroot_def.repos.values() if repo.keyring is not None
        ):
            keyring = cache.keyrings[name]
            cfg.log.debug(
                "Installing the %(name)s (%(keyring_name)s) keyring",
                {"name": name, "keyring_name": keyring.path.name},
            )
            self.copy_file(
                cfg, keyring.path, pathlib.Path("usr/share/keyrings") / keyring.path.name
            )

        repo_base: Final = cache.tempd / "repos"
        repo_base.mkdir(mode=0o755, exist_ok=True)
        first_add = True
        for name, repo in chroot_def.repos.items():
            match repo.flavor:
                case defs.RepoFlavor.URL | defs.RepoFlavor.BACKPORTS:
                    self.create_repo_file(cfg, cache, repo_base, name, repo)

                case defs.RepoFlavor.ADD_APT_REPO:
                    self.add_apt_repo(cfg, repo, first=first_add)
                    first_add = False

                case _:
                    raise NotImplementedError(repr(name, repo))

        cfg.log.debug("Running apt-get update to fetch the repository metadata")
        self.run_apt_update(cfg)

    def install_packages(
        self, cfg: defs.Config, tempd: pathlib.Path, chroot_def: defs.Chroot
    ) -> None:
        """Install any packages requested from the additional repositories."""
        all_packages: Final = sorted(
            itertools.chain(*(repo.packages for repo in chroot_def.repos.values()))
        )
        if not all_packages:
            cfg.log.debug("No additional packages to install")
            return

        cfg.log.debug("Running apt-cache policy to find the unique repository markers")
        output: Final = self.run_command(cfg, ["apt-cache", "policy"], capture_output=True)
        repos: Final = apt_output.parse_policy_repos(output)

        cfg.log.debug("Defining Apt preferences for the repositories with packages specified")
        prefs: Final = []
        for name, repo in chroot_def.repos.items():
            if not repo.packages or repo.flavor == defs.RepoFlavor.ADD_APT_REPO:
                continue
            cfg.log.debug("- %(name)s", {"name": name})

            p_repos = [p_repo for p_repo in repos if matches_policy(repo, p_repo)]
            # Sigh, `apt-cache policy` may show no entries at all for backports
            if (
                repo.flavor == defs.RepoFlavor.BACKPORTS
                and not (set(repo.components) & {p_repo.component for p_repo in p_repos})
            ) or (
                repo.flavor != defs.RepoFlavor.BACKPORTS
                and {p_repo.component for p_repo in p_repos} != set(repo.components)
            ):
                sys.exit(
                    f"Could not find `apt-cache policy` definitions for {repo!r}, got {p_repos!r}"
                )

            for pkg in repo.packages:
                for p_repo in p_repos:
                    if p_repo.release is None:
                        sys.exit(
                            f"No 'release' `apt-cache policy` output for {repo!r} in {p_repo!r}"
                        )
                    relstr = ",".join(
                        f"{key}={value}" for key, value in sorted(p_repo.release.items())
                    )
                    prefs.append(
                        f"""
Package: {pkg}
Pin: release {relstr}
Pin-Priority: 990
"""
                    )

        if prefs:
            cfg.log.debug("- generating a preferences file:\n%(prefs)s", {"prefs": prefs})
            prefs_file: Final = tempd / "ringroots.pref"
            prefs_file.write_text("".join(prefs), encoding="UTF-8")
            self.copy_file(cfg, prefs_file, pathlib.Path("etc/apt/preferences.d") / prefs_file.name)

        cfg.log.debug("- now installing some packages")
        self.run_apt_install(cfg, all_packages)

    @classmethod
    def setup_source_root_groups(
        cls, cfg: defs.Config, chroot_name: str, cfg_file: pathlib.Path
    ) -> None:
        """Allow non-root user accounts access to the schroot environment."""
        cfg.log.debug(
            "setup_source_root_groups invoked for %(chroot_name)s at %(cfg_file)s",
            {"chroot_name": chroot_name, "cfg_file": cfg_file},
        )
        cfgp: Final = configparser.ConfigParser(interpolation=None)
        try:
            cfgp.read_string(cfg_file.read_text(encoding="UTF-8"))
        except ValueError as err:
            sys.exit(f"Could not parse the {cfg_file} schroot config file: {err}")
        try:
            sect: Final = cfgp[chroot_name]
        except KeyError:
            sys.exit(
                f"Could not parse the {cfg_file} schroot config file: no '{chroot_name}' section"
            )

        try:
            root_groups: Final = sect["root-groups"].split(",")
        except KeyError:
            sys.exit(f"Expected a {chroot_name}.root-groups ine in {cfg_file}")
        cfg.log.debug("- current root-groups: %(groups)s", {"groups": root_groups})
        to_add_root: Final = [name for name in ["root", "sbuild"] if name not in root_groups]
        if to_add_root:
            root_groups.extend(to_add_root)
            cfg.log.debug("- updated root-groups: %(groups)s", {"groups": root_groups})
            sect["root-groups"] = ",".join(root_groups)

        source_root_groups: Final = (
            [] if "source-root-groups" not in sect else sect["source-root-groups"].split(",")
        )
        cfg.log.debug("- current source-root-groups: %(groups)s", {"groups": source_root_groups})
        to_add: Final = [name for name in root_groups if name not in source_root_groups]
        if not to_add:
            cfg.log.debug("- nothing to add")
            return
        source_root_groups.extend(to_add)
        cfg.log.debug("- updated source-root-groups: %(groups)s", {"groups": source_root_groups})

        sect["source-root-groups"] = ",".join(source_root_groups)

        cfg_stat: Final = cfg_file.stat()
        cfg_mode: Final = cfg_stat.st_mode & 0o7777
        with tempfile.NamedTemporaryFile(
            prefix="ringroots-chroot.", mode="wt", encoding="UTF-8"
        ) as tempf_obj:
            cfgp.write(tempf_obj, space_around_delimiters=False)
            tempf_obj.flush()
            subprocess.check_call(
                [
                    cfg.sudo,
                    "--",
                    "install",
                    "-o",
                    str(cfg_stat.st_uid),
                    "-g",
                    str(cfg_stat.st_gid),
                    "-m",
                    f"{cfg_mode:o}",
                    "--",
                    tempf_obj.name,
                    cfg_file,
                ],
                env=cfg.utf8_env,
            )

    @classmethod
    def _create_schroot(cls, cfg: defs.Config, chroot_def: defs.Chroot) -> Self:
        """Set up the base schroot environment."""
        base_name, arch = chroot_def.split_name()
        prop: Final = defs.CHROOT_DEFS[(chroot_def.distro, chroot_def.release)]
        if not isinstance(prop, defs.SChrootChrootMeta):
            raise TypeError(repr((chroot_def, prop)))
        cfg.log.debug(
            "Creating the base %(base_name)s/%(arch)s chroot environment with%(merged)s merged-usr",
            {"base_name": base_name, "arch": arch, "merged": "" if prop.merged_usr else "out"},
        )

        mirror: Final = cfg.mirrors[chroot_def.distro][defs.MirrorType.MAIN]

        def extra_repo(repo_mirror: str | None, suffix: str) -> list[str]:
            """Get the repository definitions for additional repositories."""
            if repo_mirror is None:
                return []

            return [
                (
                    f"--extra-repository=deb {repo_mirror} {chroot_def.release}-{suffix}"
                    f" {' '.join(prop.components)}"
                ),
                (
                    f"--extra-repository=deb-src {repo_mirror} {chroot_def.release}-{suffix}"
                    f" {' '.join(prop.components)}"
                ),
            ]

        updates_repo: Final = extra_repo(mirror, "updates") if prop.add_updates else []
        security_repo: Final = extra_repo(prop.security_mirror_override(mirror), "security")
        subprocess.check_call(
            [
                cfg.sudo,
                "--",
                "sbuild-createchroot",
                f"--arch={arch}",
                f"--chroot-prefix={base_name}",
                "--chroot-suffix=",
                "--merged-usr" if prop.merged_usr else "--no-merged-usr",
                f"--components={','.join(prop.components)}",
                *updates_repo,
                *security_repo,
                "--",
                chroot_def.release,
                chroot_def.get_chroot_path(),
                mirror,
            ],
            env=cfg.utf8_env,
        )

        cfg_file: Final = chroot_def.get_def_path()
        cfg.log.debug(
            "Enabling source access in the %(cfg_file)s config file", {"cfg_file": cfg_file}
        )
        cls.setup_source_root_groups(cfg, chroot_def.name, cfg_file)

        cfg.log.debug(
            "Checking the contents of the %(cfg_file)s config file", {"cfg_file": cfg_file}
        )
        if cfg.verbose:
            cfg.log.debug("%(contents)s", {"contents": cfg_file.read_text(encoding="UTF-8")})

        cfg.log.debug("Obtaining the list of schroot environments once again")
        schroots: Final = cls.list_all(cfg)
        schroot: Final = schroots.get(chroot_def.name)
        if schroot is None:
            sys.exit(
                f"Could not find the {chroot_def.name} schroot after supposedly having created it"
            )
        return schroot

    @classmethod
    def setup_modprobe_wrapper(cls, cfg: defs.Config) -> pathlib.Path | None:
        """Create a symlink for `/sbin/modprobe` if it does not exist.

        The `sbuild-createchroot` tool tries to invoke it unconditionally.
        """
        modprobe_path: Final = pathlib.Path("/sbin/modprobe")
        if modprobe_path.exists() or modprobe_path.is_symlink():
            return None

        subprocess.check_call(
            [cfg.sudo, "--", "ln", "-s", "--", "/bin/true", modprobe_path], env=cfg.utf8_env
        )
        return modprobe_path

    @classmethod
    def create(
        cls,
        cfg: defs.Config,
        cache: defs.Cache,
        chroot_def: defs.Chroot,
    ) -> Self:
        """Create a single chroot environment."""
        cfg.log.debug(
            "Creating the %(name)s (%(distro)s/%(release)s) chroot environment",
            {"name": chroot_def.name, "distro": chroot_def.distro, "release": chroot_def.release},
        )

        for name in (
            repo.keyring for repo in chroot_def.repos.values() if repo.keyring is not None
        ):
            fetch.ensure_keyring(cfg, cache, name)

        modprobe_wrapped: Final = cls.setup_modprobe_wrapper(cfg)
        try:
            schroot: Final = cls._create_schroot(cfg, chroot_def)
        finally:
            if modprobe_wrapped is not None:
                subprocess.check_call(
                    [cfg.sudo, "--", "rm", "--", modprobe_wrapped], env=cfg.utf8_env
                )
        schroot.setup_repos(cfg, cache, chroot_def)
        schroot.install_packages(cfg, cache.tempd, chroot_def)
        return schroot

    @classmethod
    def remove(cls, cfg: defs.Config, chroot: defs.Chroot) -> None:
        """Remove all files pertaining to the schroot environment."""
        cfg.log.debug(
            "Removing the %(name)s chroot environment and all files pertaining to it",
            {"name": chroot.name},
        )

        schroots: Final = cls.list_all(cfg)
        sessions: Final = SChrootSession.list_all(cfg)

        schroot: Final = schroots.get(chroot.name)
        if schroot is not None:
            cfg.log.debug(
                "The %(schroot_name)s chroot exists; looking for running sessions",
                {"schroot_name": schroot.name},
            )
            found = ",".join(
                sorted(sess.name for sess in sessions.values() if sess.original_name == chroot.name)
            )
            if found:
                sys.exit(f"Cannot remove the {schroot.name} schroot: running sessions: {found}")

        sdir: Final = chroot.get_chroot_path()
        if sdir.is_dir():
            cfg.log.debug("Removing the %(sdir)s chroot directory", {"sdir": sdir})
            try:
                subprocess.check_call([cfg.sudo, "--", "rm", "-rf", "--", sdir], env=cfg.utf8_env)
            except (OSError, subprocess.CalledProcessError) as err:
                sys.exit(f"Could not remove the {sdir} chroot directory: {err}")
        else:
            cfg.log.debug("No %(sdir)s directory to remove", {"sdir": sdir})

        cfg_file: Final = chroot.get_def_path()
        if cfg_file.exists():
            cfg.log.debug(
                "Removing the %(cfg_file)s schroot definition file", {"cfg_file": cfg_file}
            )
            try:
                subprocess.check_call([cfg.sudo, "--", "rm", "--", cfg_file], env=cfg.utf8_env)
            except (OSError, subprocess.CalledProcessError) as err:
                sys.exit(f"Could not remove the {cfg_file} schroot definition file: {err}")
        else:
            cfg.log.debug("No %(cfg_file)s file to remove", {"cfg_file": cfg_file})

    def run_in_session(
        self, cfg: defs.Config, func: Callable[[str], None], *, source: bool = False
    ) -> None:
        """Start a chroot session, invoke a function, end the session."""
        name: Final = ("source:" if source else "") + self.name
        cfg.log.debug("Starting a session for the %(name)s schroot environment", {"name": name})

        sid_lines: Final = subprocess.check_output(
            ["schroot", "-b", "-c", name, "-u", "root", "-d", "/"],
            encoding="UTF-8",
            env=cfg.utf8_env,
        ).splitlines()
        if len(sid_lines) != 1:
            sys.exit(f"Unexpected output from `schroot -b {name}`: {sid_lines!r}")
        sid: Final = sid_lines[0]
        cfg.log.debug("Got schroot session id %(sid)s", {"sid": sid})

        try:
            func(sid)
        finally:
            cfg.log.debug("Stopping the %(sid)s schroot session", {"sid": sid})
            subprocess.check_call(["schroot", "-e", "-c", sid], env=cfg.utf8_env)

    def update(self, cfg: defs.Config) -> None:
        """Update the packages in a schroot's source environment."""

        def do_update(sid: str) -> None:
            """Update the packages in the current session."""
            cfg.log.debug("Running `apt-get update`")
            self.run_apt_update(cfg, sid=sid)

            cfg.log.debug("Running `apt-get dist-upgrade`")
            self.run_apt_upgrade(cfg, sid=sid)

            cfg.log.debug("Running `apt-get clean`")
            self.run_apt_clean(cfg, sid=sid)

        cfg.log.debug(
            "Updating the %(name)s source schroot environment's packages", {"name": self.name}
        )
        self.run_in_session(cfg, do_update, source=True)


@dataclasses.dataclass(frozen=True)
class SChrootSession(SChroot):
    """A running schroot session."""

    original_name: str
    """The name of the schroot environment that this session was spawned from."""

    @staticmethod
    def _blurb() -> str:
        return "schroot session"

    @staticmethod
    def _list_cmdline_option() -> str:
        return "--all-sessions"

    @classmethod
    def _parse_list_section(
        cls, _cfg: defs.Config, name: str, section: configparser.SectionProxy
    ) -> Self:
        """Parse a single schroot definition."""
        try:
            return cls(
                name=name,
                directory=pathlib.Path(section["directory"]),
                original_name=section["original-name"],
            )
        except (KeyError, ValueError) as err:
            sys.exit(f"Could not parse the {name} schroot session configuration: {err}")
