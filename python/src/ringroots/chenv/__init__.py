# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Multiplex among the chroot environment handlers."""

from __future__ import annotations

import itertools
import pathlib
import tempfile
import typing

from ringroots import defs

from . import mock as h_mock
from . import schroot as h_schroot


if typing.TYPE_CHECKING:
    from typing import Final


def create_chroots(cfg: defs.Config, chroots: list[defs.Chroot]) -> None:
    """Create the configured chroot environments."""
    with tempfile.TemporaryDirectory() as tempd_obj:
        tempd: Final = pathlib.Path(tempd_obj)
        cache: Final = defs.Cache(tempd=tempd, keyring_defs=cfg.keyrings, keyrings={})
        for chroot in chroots:
            match chroot.get_meta().flavor:
                case defs.ChrootFlavor.SCHROOT:
                    h_schroot.SChroot.create(cfg, cache, chroot)

                case defs.ChrootFlavor.MOCK:
                    h_mock.Mock.create(cfg, cache, chroot)

                case _:
                    raise NotImplementedError(repr(chroot))


def list_all(cfg: defs.Config) -> dict[str, defs.ChEnv]:
    """List defined chroot environments that exist."""
    return dict(
        itertools.chain(h_schroot.SChroot.list_all(cfg).items(), h_mock.Mock.list_all(cfg).items())
    )


def remove_chroot(cfg: defs.Config, chroot: defs.Chroot) -> None:
    """Remove the specified chroot environment."""
    match chroot.get_meta().flavor:
        case defs.ChrootFlavor.SCHROOT:
            h_schroot.SChroot.remove(cfg, chroot)

        case defs.ChrootFlavor.MOCK:
            h_mock.Mock.remove(cfg, chroot)

        case _:
            raise NotImplementedError(repr(chroot))
