# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Handle schroot environments."""

from __future__ import annotations

import dataclasses
import pathlib
import subprocess  # noqa: S404
import sys
import typing
import urllib.parse

from ringroots import defs


if typing.TYPE_CHECKING:
    from typing import Final

    if sys.version_info >= (3, 11):
        from typing import Self
    else:
        from typing_extensions import Self


MOCK_CONFIG: Final = pathlib.Path("/etc/mock")
"""The base path for the mock configuration files tree."""

DISTRIB_KEYS: Final = pathlib.Path("/usr/share/distribution-gpg-keys")
"""The base path for the RPM distribution keys directory that Mock requires."""


@dataclasses.dataclass
class ConfigExistsError(defs.Error):
    """A mock configuration file already exists."""

    path: pathlib.Path
    """The path to the config file that we did not expect to exist."""

    def __str__(self) -> str:
        """Provide a human-readable representation."""
        return f"The {self.path} mock configuration already exists"


@dataclasses.dataclass
class MockNotFoundError(defs.Error):
    """Could not find the just-created mock environment."""

    name: str
    """The name of the mock environment we tried to create."""

    def __str__(self) -> str:
        """Provide a human-readable representation."""
        return f"Could not find the {self.name} mock environment after supposedly having created it"


@dataclasses.dataclass
class KeyringDistribOnlyError(defs.Error):
    """Only distribution keys supported."""

    keyring: str
    """The name of the keyring that triggered the error."""

    url: str
    """The URL that did not match."""

    def __str__(self) -> str:
        """Provide a human-readable representation."""
        return f"Keyring {self.keyring}: URL {self.url}: must be a file within {DISTRIB_KEYS}"


def add_url_repo(cfg: defs.Config, name: str, repo: defs.DNFRepo, *, source: bool) -> str:
    """Add the definition of a single repository."""
    source_url: Final = repo.source + ("" if repo.source.endswith("/") else "/")
    name_slug: Final = "-source" if source else ""
    enabled_slug: Final = "0" if source else "1"
    url_slug: Final = "SRPMS" if source else "$basearch"

    if repo.keyring is None:
        gpg_lines = """
gpgcheck=0
"""
    else:
        keyring: Final = cfg.keyrings[repo.keyring]
        url_parts: Final = urllib.parse.urlparse(keyring.url)
        if url_parts.scheme != "file":
            raise KeyringDistribOnlyError(repo.keyring, keyring.url)
        keyring_path: Final = pathlib.Path(url_parts.path)
        if not keyring_path.is_relative_to(DISTRIB_KEYS):
            raise KeyringDistribOnlyError(repo.keyring, keyring.url)
        if not keyring_path.is_file():
            raise KeyringDistribOnlyError(repo.keyring, keyring.url)
        gpg_lines = f"""
gpgcheck=1
gpgkey={keyring.url}
"""

    return f"""
[{name}{name_slug}]
name={repo.comment}
baseurl={source_url}$releasever/{url_slug}/
enabled={enabled_slug}
{gpg_lines.lstrip()}

""".lstrip()


@dataclasses.dataclass(frozen=True)
class Mock(defs.ChEnv):
    """A single existing mock environment."""

    @staticmethod
    def _blurb() -> str:
        """Provide a short descriptive string."""
        return "mock environment"

    @classmethod
    def get(cls, cfg: defs.Config, name: str) -> Self | None:
        """Get the properties of a the specified mock environment if it exists."""
        cfg.log.debug("Looking for the %(name)s %(blurb)s", {"name": name, "blurb": cls._blurb()})
        res = subprocess.run(
            ["mock", "--root", name, "--print-root-path"],
            capture_output=True,
            check=False,
            encoding="UTF-8",
            env=cfg.utf8_env,
        )
        if res.returncode:
            return None

        if res.stdout is None:
            raise RuntimeError(repr((name, res)))
        lines = res.stdout.splitlines()
        match lines:
            case [single]:
                return cls(name=name, directory=pathlib.Path(single))

            case _:
                raise RuntimeError(repr((name, res)))

    @classmethod
    def list_all(cls, cfg: defs.Config) -> dict[str, Self]:
        """List all the existing mock environments."""
        cfg.log.debug("Looking for any existing %(blurb)ss", {"blurb": cls._blurb()})
        defined: Final = [
            chroot.name
            for chroot in cfg.chroots
            if chroot.get_meta().flavor == defs.ChrootFlavor.MOCK
        ]
        return {
            name: env
            for (name, env) in ((name, cls.get(cfg, name)) for name in defined)
            if env is not None
        }

    @classmethod
    def create(
        cls,
        cfg: defs.Config,
        cache: defs.Cache,
        chroot_def: defs.Chroot,
    ) -> Self:
        """Create a single chroot environment."""
        mock_cfg: Final = MOCK_CONFIG / f"{chroot_def.name}.cfg"
        if mock_cfg.exists() or mock_cfg.is_symlink():
            raise ConfigExistsError(mock_cfg)

        base_os: Final = "centos" if chroot_def.release == "7" else "almalinux"
        cfg.log.debug(
            "Creating %(mock_cfg)s %(blurb)s config based on %(base_os)s version %(release)s",
            {
                "mock_cfg": str(mock_cfg),
                "blurb": cls._blurb(),
                "base_os": base_os,
                "release": chroot_def.release,
            },
        )
        mock_cfg_text = f"""
include('{base_os}-{chroot_def.release}-{chroot_def.get_arch()}.cfg')
include('templates/epel-{chroot_def.release}.tpl')

config_opts['root'] = '{chroot_def.name}'

"""
        if chroot_def.repos:
            mock_cfg_text += '''

config_opts['yum.conf'] += """
'''
            for name, repo in chroot_def.repos.items():
                if not isinstance(repo, defs.DNFRepo):
                    raise TypeError(repr(repo))

                match repo.flavor:
                    case defs.RepoFlavor.URL:
                        if defs.PackageType.RPM in repo.types:
                            mock_cfg_text += add_url_repo(cfg, name, repo, source=False)

                        if defs.PackageType.RPM_SRC in repo.types:
                            mock_cfg_text += add_url_repo(cfg, name, repo, source=True)

                    case _:
                        raise NotImplementedError(repr((name, repo)))

            mock_cfg_text += '"""\n'

        cfg.log.info("Creating the %(mock_cfg)s mock config file", {"mock_cfg": mock_cfg})
        mock_temp: Final = cache.tempd / "mock-cfg" / mock_cfg.name
        mock_temp.parent.mkdir(mode=0o755, exist_ok=True)
        mock_temp.write_text(mock_cfg_text, encoding="UTF-8")
        subprocess.check_call(
            [
                cfg.sudo,
                "--",
                "install",
                "-o",
                "root",
                "-g",
                "root",
                "-m",
                "644",
                "--",
                mock_temp,
                mock_cfg,
            ],
            env=cfg.utf8_env,
        )

        cfg.log.info("Invoking mock to bootstrap %(name)s", {"name": chroot_def.name})
        subprocess.check_call(["mock", "--root", chroot_def.name, "--init"], env=cfg.utf8_env)

        mock: Final = cls.get(cfg, chroot_def.name)
        if mock is None:
            raise MockNotFoundError(chroot_def.name)
        return mock

    @classmethod
    def remove(cls, cfg: defs.Config, chroot_def: defs.Chroot) -> None:
        """Remove the mock environment and its configuration files."""
        mock_cfg: Final = MOCK_CONFIG / f"{chroot_def.name}.cfg"
        if not mock_cfg.exists() and not mock_cfg.is_symlink():
            cfg.log.info("The %(mock_cfg)s mock config file does not exist", {"mock_cfg": mock_cfg})
            return

        cfg.log.info("Removing the %(name)s mock chroot environment", {"name": chroot_def.name})
        res: Final = subprocess.run(
            ["mock", "--root", chroot_def.name, "--scrub", "all"], check=False, env=cfg.utf8_env
        )
        if res.returncode:
            cfg.log.warning(
                "mock failed to remove the %(name)s chroot environment", {"name": chroot_def.name}
            )

        cfg.log.info("Removing the %(mock_cfg)s mock config file", {"mock_cfg": mock_cfg})
        subprocess.check_call([cfg.sudo, "--", "rm", "--", mock_cfg], env=cfg.utf8_env)

    def update(self, cfg: defs.Config) -> None:
        """Update the specified chroot environment."""
        cfg.log.info("Recreating the %(name)s mock chroot environment", {"name": self.name})
        res_scrub: Final = subprocess.run(
            ["mock", "--root", self.name, "--scrub", "bootstrap", "--scrub", "chroot"],
            check=False,
            env=cfg.utf8_env,
        )
        if res_scrub.returncode:
            sys.exit(f"mock failed to remove the {self.name} chroot environment")

        res_init: Final = subprocess.run(
            ["mock", "--root", self.name, "--init"], check=False, env=cfg.utf8_env
        )
        if res_init.returncode:
            sys.exit(f"mock failed to create the {self.name} chroot environment")
