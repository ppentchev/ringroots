# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""A helper tool for creating and updating chroot environments."""

from __future__ import annotations

import dataclasses
import pathlib
import sys
import typing

import click
import utf8_locale

from . import chenv
from . import defs
from . import parse
from . import utils


if typing.TYPE_CHECKING:
    from typing import Final


@dataclasses.dataclass
class ConfigHolder:
    """Hold a `Config` object."""

    cfg: defs.Config | None = None
    """The `Config` object stashed by the main function."""


def extract_cfg(ctx: click.Context) -> defs.Config:
    """Extract the `Config` object that the main function built."""
    cfg_hold: Final = ctx.find_object(ConfigHolder)
    if cfg_hold is None:
        sys.exit("Internal error: no click config holder object")

    cfg: Final = cfg_hold.cfg
    if cfg is None:
        sys.exit("Internal error: no config in the click config holder")

    return cfg


# The boolean argument is part of click's API.
def arg_features(_ctx: click.Context, _self: click.Parameter, value: bool) -> bool:  # noqa: FBT001
    """Display program features information and exit."""
    if not value:
        return value

    print("Features: " + " ".join(f"{name}={value}" for name, value in defs.FEATURES.items()))
    sys.exit(0)


@click.command(name="create")
@click.option("--missing", "-M", is_flag=True, help="create all the missing environments")
@click.argument("names", type=str, nargs=-1)
@click.pass_context
def cmd_create(ctx: click.Context, *, missing: bool, names: list[str]) -> None:
    """Create one or more chroot environments."""
    cfg: Final = extract_cfg(ctx)

    existing: Final = chenv.list_all(cfg)

    def build_chroots() -> list[defs.Chroot]:
        """Build up the list of chroot environments to operate on."""
        nonlocal missing, names
        match missing, names:
            case True, []:
                chroots = [chroot for chroot in cfg.chroots if chroot.name not in existing]

            case True, _:
                sys.exit("The --missing option cannot be specified along with a list of names")

            case False, []:
                sys.exit("Neither any chroot names nor the --missing option was specified")

            case False, names:

                def add_chroot(name: str) -> defs.Chroot:
                    """Validate the name of a single chroot environment."""
                    chroot = cfg.chroots_by_name.get(name)
                    if chroot is None:
                        sys.exit(f"Undefined chroot environment '{name}'")
                    if name in existing:
                        sys.exit(f"The {name} chroot environment already exists")
                    return chroot

                chroots = [add_chroot(name) for name in names]

        return chroots

    chroots: Final = build_chroots()
    if not chroots:
        print("No need to create any chroot environments")
        return

    chenv.create_chroots(cfg, chroots)


@click.command(name="list")
@click.option("--check", is_flag=True, help="check whether the chroot environments exist")
@click.pass_context
def cmd_list(ctx: click.Context, *, check: bool) -> None:
    """List the available chroot environments."""
    cfg: Final = extract_cfg(ctx)

    if check:
        present: Final = set(chenv.list_all(cfg))

    for chroot in sorted(cfg.chroots, key=lambda chroot: chroot.name):
        if check:
            checked = "yes" if chroot.name in present else "no"
            print(f"{chroot.name}\t{chroot.distro}/{chroot.release}\t{checked}")
        else:
            print(f"{chroot.name}\t{chroot.distro}/{chroot.release}")


@click.command(name="remove")
@click.argument("name", type=str, required=True)
@click.pass_context
def cmd_remove(ctx: click.Context, *, name: str) -> None:
    """Remove the specified chroot environment and all files pertaining to it."""
    cfg: Final = extract_cfg(ctx)

    chroot: Final = cfg.chroots_by_name.get(name)
    if chroot is None:
        sys.exit(f"Undefined chroot environment '{name}'")

    chenv.remove_chroot(cfg, chroot)


@click.command(name="update")
@click.option("--existing", "-E", is_flag=True, help="update all the existing environments")
@click.argument("names", type=str, nargs=-1)
@click.pass_context
def cmd_update(ctx: click.Context, *, existing: bool, names: list[str]) -> None:
    """Update one or more chroot environments."""
    cfg: Final = extract_cfg(ctx)

    present: Final = chenv.list_all(cfg)

    def build_chroots() -> list[defs.Chroot]:
        """Build up the list of chroot environments to operate on."""
        nonlocal existing, names
        match existing, names:
            case True, []:
                chroots = [chroot for chroot in cfg.chroots if chroot.name in present]

            case True, _:
                sys.exit("The --existing option cannot be specified along with a list of names")

            case False, []:
                sys.exit("No chroot names and no --existing option specified")

            case False, names:

                def add_chroot(name: str) -> defs.Chroot:
                    """Validate the name of a single chroot environment."""
                    chroot = cfg.chroots_by_name.get(name)
                    if chroot is None:
                        sys.exit(f"Undefined chroot environment '{name}'")
                    if name not in present:
                        sys.exit(f"The {name} chroot environment does not exist")
                    return chroot

                chroots = [add_chroot(name) for name in names]

        return chroots

    for chroot in build_chroots():
        present[chroot.name].update(cfg)


def get_default_config_file() -> pathlib.Path:
    """Get the path to the default config file in the user's home directory."""
    home_dir: Final = utils.find_home_dir()
    return home_dir / ".config/ringroots.toml"


@click.group(name="ringroots")
@click.option("--features", is_flag=True, is_eager=True, callback=arg_features)
@click.option(
    "--config-file",
    "-f",
    type=click.Path(
        exists=True, file_okay=True, dir_okay=False, resolve_path=True, path_type=pathlib.Path
    ),
    default=get_default_config_file,
    help="specify a configuration file to use instead of the default one",
)
@click.option("--quiet", "-q", is_flag=True, help="quiet operation; suppress diagnostic output")
@click.option(
    "--sudo",
    "-s",
    type=str,
    default="sudo",
    help="the command (a single program name) to use for running commands as root; default: sudo",
)
@click.pass_context
def main(
    ctx: click.Context, *, config_file: pathlib.Path, features: bool, quiet: bool, sudo: str
) -> None:
    """Parse command-line arguments, run commands."""
    if features:
        sys.exit("Internal error: why did we get to main() with features=True?")

    cfg_data: Final = parse.parse_config_file(config_file)

    ctx.ensure_object(ConfigHolder)
    ctx.obj.cfg = defs.Config(
        chroots=cfg_data.chroots,
        chroots_by_name={chroot.name: chroot for chroot in cfg_data.chroots},
        keyrings=cfg_data.keyrings,
        log=utils.build_logger(verbose=not quiet),
        mirrors=cfg_data.mirrors,
        config_file=config_file,
        sudo=sudo,
        utf8_env=utf8_locale.UTF8Detect().detect().env,
        verbose=not quiet,
    )


main.add_command(cmd_create)
main.add_command(cmd_list)
main.add_command(cmd_remove)
main.add_command(cmd_update)


if __name__ == "__main__":
    main()
