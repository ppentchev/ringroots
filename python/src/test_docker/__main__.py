# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Test some of ringroots's functionality in a Docker container."""

from __future__ import annotations

import contextlib
import dataclasses
import itertools
import pathlib
import posix
import re
import shlex
import subprocess  # noqa: S404
import sys
import tempfile
import typing

import click
import utf8_locale

from ringroots import utils


if typing.TYPE_CHECKING:
    import logging
    from collections.abc import Iterator
    from typing import Final


PathList = list[pathlib.Path | str]


@dataclasses.dataclass(frozen=True)
class ChrootDef:
    """The definition of a test chroot environment."""

    name: str
    distro: str
    release: str
    version_id: str


@dataclasses.dataclass(frozen=True)
class Container:
    """A Docker container that was started."""

    cid: str
    homedir: pathlib.Path
    sbuild_group: bool
    utf8_env: dict[str, str]

    def with_sbuild_group(self, sbuild_group: bool) -> Container:  # noqa: FBT001
        """Copy the values to a new Container object, update the sbuild_group field."""
        return dataclasses.replace(self, sbuild_group=sbuild_group)

    def base_cmd(self, *, as_root: bool = False, detach: bool = False) -> PathList:
        """Get the base command for running things in this container."""
        username: Final = "root" if as_root else USERNAME
        groupname: Final = "sbuild" if self.sbuild_group else username
        base: Final[PathList] = [
            "docker",
            "exec",
            "-u",
            f"{username}:{groupname}",
            "-w",
            "/",
        ]
        return (
            base
            + (["-d"] if detach else [])
            + [
                "--",
                self.cid,
            ]
        )

    def conffile(self) -> pathlib.Path:
        """Return the path to the ringroots config file."""
        return self.homedir / ".config/ringroots.toml"

    def check_call(self, cmd: PathList, *, as_root: bool = False, detach: bool = False) -> None:
        """Run a command in the container."""
        subprocess.check_call(
            self.base_cmd(as_root=as_root, detach=detach) + cmd, env=self.utf8_env
        )

    # Yes, we really do want to use "input" as a parameter name.
    def run_pipe_in(self, cmd: PathList, *, input: str) -> None:  # noqa: A002
        """Run a command, send the specified string to its standard input."""
        subprocess.run(
            self.base_cmd() + cmd, check=True, encoding="UTF-8", env=self.utf8_env, input=input
        )

    def run_capture(self, cmd: PathList) -> tuple[list[str], list[str]]:
        """Capture the output of a command run in the container."""
        res: Final = subprocess.run(
            self.base_cmd() + cmd,
            capture_output=True,
            check=False,
            encoding="UTF-8",
            env=self.utf8_env,
        )
        if res.returncode != 0:
            sys.exit(
                f"Failed: {shlex.join(map(str, cmd))} -- exit code {res.returncode}, "
                f"output {res.stdout!r}, errors {res.stderr!r}"
            )
        return res.stdout.splitlines(), res.stderr.splitlines()


@dataclasses.dataclass(frozen=True)
class Mode:
    """The base of the runtime mode classes."""


@dataclasses.dataclass(frozen=True)
class ModeRun(Mode):
    """Run the test."""


@dataclasses.dataclass(frozen=True)
class Config:
    """Runtime configuration for the Docker test tool."""

    group_id: int
    image: str
    log: logging.Logger
    python_version: str
    srcdir: pathlib.Path
    user_id: int
    utf8_env: dict[str, str]


CHROOTS: Final = [
    ChrootDef(name="unstable-amd64", distro="debian", release="unstable", version_id=""),
    ChrootDef(name="ringlet-stable-amd64", distro="debian", release="bullseye", version_id="11"),
    ChrootDef(name="focal-amd64", distro="ubuntu", release="focal", version_id="20.04"),
    ChrootDef(name="weird-i386", distro="debian", release="unstable", version_id=""),
]

MIRROR: Final = "http://10.42.6.1:9999"

RE_CID: Final = re.compile(r"^ (?P<id> [a-f0-9]+ ) $", re.X)

RE_DISTRO_VERSION: Final = re.compile(
    r"""
    ^
    i (?P<distro> [A-Za-z0-9_.-]+ )
    \s
    v (?P<version_id> [A-Za-z0-9_.-]* )
    $
    """,
    re.X,
)

USERNAME: Final = "jrl"


@contextlib.contextmanager
def run_container(cfg: Config, *, homedir: pathlib.Path) -> Iterator[Container]:
    """Start a Docker container, then eventually stop it."""
    img_name: Final = f"debian:{cfg.image}"
    cfg.log.debug("Starting a %(img_name)s container", {"img_name": img_name})
    try:
        lines: Final = subprocess.check_output(
            [
                "docker",
                "run",
                "--privileged",
                "-d",
                "-e",
                "LC_ALL=C.UTF-8",
                "-e",
                "LANGUAGE=",
                "--init",
                "--pull",
                "never",
                "--rm",
                "-v",
                f"{cfg.srcdir}:/opt/ringroots:ro",
                "-v",
                f"{homedir}:/home/{USERNAME}",
                "--",
                img_name,
                "sleep",
                "3600",
            ],
            encoding="UTF-8",
            env=cfg.utf8_env,
        ).splitlines()
    except (OSError, subprocess.CalledProcessError) as err:
        sys.exit(f"Could not start a {img_name} Docker container: {err}")

    if len(lines) != 1:
        sys.exit(
            f"Unexpected number of lines from `docker run`: expected a single line, got {lines!r}"
        )
    cid_m: Final = RE_CID.match(lines[0])
    if not cid_m:
        sys.exit(f"Unexpected output from `docker run`: expected a container ID, got {lines[0]!r}")
    cid: Final = cid_m.group("id")
    cfg.log.debug("Got container ID %(cid)s", {"cid": cid})

    try:
        yield Container(cid=cid, homedir=homedir, sbuild_group=False, utf8_env=cfg.utf8_env)
    finally:
        cfg.log.debug("Stopping the %(cid)s container", {"cid": cid})
        try:
            subprocess.check_call(["docker", "stop", "--", cid], env=cfg.utf8_env)
        except (OSError, subprocess.CalledProcessError) as err:
            sys.exit(f"Could not stop the {cid} Docker container: {err}")


def update_sources_list(cfg: Config, prep_cont: Container, sources_fname: str) -> None:
    """Update the old-style /etc/apt/sources.list file."""
    cfg.log.debug("What does %(fname)s say now?", {"fname": sources_fname})
    prep_cont.check_call(["cat", "--", sources_fname])

    cfg.log.debug("Modifying %(fname)s", {"fname": sources_fname})
    prep_cont.check_call(
        [
            "sed",
            "-i",
            "-e",
            f"s@http://deb.debian.org/@{MIRROR}/@",
            "--",
            sources_fname,
        ],
        as_root=True,
    )

    cfg.log.debug("Did we do it? Did we? Did we?")
    prep_cont.check_call(["cat", "--", sources_fname])


def update_sources_file(cfg: Config, prep_cont: Container, sources_fname: str) -> None:
    """Update the new debian.sources file."""
    cfg.log.debug("What does %(fname)s say now?", {"fname": sources_fname})
    prep_cont.check_call(["cat", "--", sources_fname])

    cfg.log.debug("Modifying %(fname)s", {"fname": sources_fname})
    prep_cont.check_call(
        [
            "sed",
            "-i",
            "-e",
            f"s@URIs: http://deb.debian.org/debian$@URIs: {MIRROR}/debian@",
            "--",
            sources_fname,
        ],
        as_root=True,
    )

    cfg.log.debug("Did we do it? Did we? Did we?")
    prep_cont.check_call(["cat", "--", sources_fname])


def detect_sources_list(cfg: Config, prep_cont: Container) -> None:
    """Update either the old-style sources.list file or the new debian.sources one."""
    lines_stdout, lines_stderr = prep_cont.run_capture(
        [
            "find",
            "/etc/apt/",
            "-type",
            "f",
            "(",
            "-name",
            "sources.list",
            "-or",
            "-name",
            "debian.sources",
            ")",
        ]
    )
    if lines_stderr:
        sys.exit(f"Looking for the Apt sources list caused errors: {lines_stderr!r}")
    if len(lines_stdout) != 1:
        sys.exit(f"Expected exactly one standard Apt sources list file, got {lines_stdout!r}")
    sources_fname: Final = lines_stdout[0]
    sources_path: Final = pathlib.Path(sources_fname)

    if sources_path.suffix == ".list":
        update_sources_list(cfg, prep_cont, sources_fname)
    elif sources_path.suffix == ".sources":
        update_sources_file(cfg, prep_cont, sources_fname)
    else:
        sys.exit(f"Internal error: found a weird Apt sources list file: {sources_fname}")


def prepare_container(cfg: Config, prep_cont: Container) -> Container:
    """Install the necessary packages, configure some of them."""
    cfg.log.debug("Create the user group")
    prep_cont.check_call(["groupadd", "-g", str(cfg.group_id), "--", USERNAME], as_root=True)

    cfg.log.debug("Create the user account")
    prep_cont.check_call(
        [
            "useradd",
            "-c",
            "Just a random user",
            "-g",
            USERNAME,
            "-M",
            "-s",
            "/bin/sh",
            "-u",
            str(cfg.user_id),
            "--",
            USERNAME,
        ],
        as_root=True,
    )

    detect_sources_list(cfg, prep_cont)

    cfg.log.debug("Updating the Apt database")
    prep_cont.check_call(["apt-get", "update"], as_root=True)

    cfg.log.debug("Installing a couple of packages")
    prep_cont.check_call(
        [
            "env",
            "DEBIAN_FRONTEND=noninteractive",
            "apt-get",
            "install",
            "-y",
            "--no-install-recommends",
            "debootstrap",
            "dma",
            f"python{cfg.python_version}",
            "python3-click",
            "python3-pyparsing",
            "python3-tomli",
            "python3-typedload",
            "python3-utf8-locale",
            "sbuild",
            "schroot",
            "sudo",
        ],
        as_root=True,
    )

    cfg.log.debug(
        "Setting up the sudo access rights for the %(username)s account", {"username": USERNAME}
    )

    (prep_cont.homedir / "ring-nopasswd.sudo").write_text(
        f"{USERNAME} ALL = (ALL) NOPASSWD: ALL\n", encoding="UTF-8"
    )
    prep_cont.check_call(
        [
            "install",
            "-o",
            "root",
            "-g",
            "root",
            "-m",
            "440",
            "/home/jrl/ring-nopasswd.sudo",
            "/etc/sudoers.d/ring-nopasswd",
        ],
        as_root=True,
    )

    prep_cont.check_call(["sudo", "-l"])

    prep_cont.check_call(["sudo", "usermod", "-a", "-G", "sbuild", "--", "root"])
    prep_cont.check_call(["sudo", "usermod", "-a", "-G", "sbuild", "--", USERNAME])

    cont = prep_cont.with_sbuild_group(True)  # noqa: FBT003

    (cont.homedir / ".sbuildrc").write_text(
        f"""
$mailto = '{USERNAME}';
1;
""",
        encoding="UTF-8",
    )
    cont.check_call(
        [
            "sudo",
            "install",
            "-o",
            "root",
            "-g",
            "root",
            "-m",
            "644",
            f"/home/{USERNAME}/.sbuildrc",
            "/root/.sbuildrc",
        ]
    )

    cont.conffile().parent.mkdir(mode=0o755, parents=True)
    cont.conffile().write_text(
        f"""[format.version]
major = 0
minor = 2

[mirrors]
debian = "{MIRROR}/debian"
ubuntu = "{MIRROR}/ubuntu"
"""
        + "".join(
            f"""
[chroots.{chroot.name}]
distro = "{chroot.distro}"
release = "{chroot.release}"
"""
            for chroot in CHROOTS
        ),
        encoding="UTF-8",
    )

    return cont


def ringroots_base(cfg: Config) -> PathList:
    """Return the base command for running ringroots in the container."""
    return ["env", f"PYTHON3=python{cfg.python_version}", "/opt/ringroots/bin/ringroots"]


def run_ringroots(cfg: Config, cont: Container, args: PathList) -> None:
    """Run 'ringroots' within the container with the specified list of arguments."""
    cont.check_call(ringroots_base(cfg) + args)


def run_ringroots_capture(
    cfg: Config, cont: Container, args: PathList
) -> tuple[list[str], list[str]]:
    """Run 'ringroots' within the container with the specified list of arguments."""
    return cont.run_capture(ringroots_base(cfg) + args)


def test_list(cfg: Config, cont: Container, exist: list[bool]) -> None:
    """Test the output of 'ringroots list'."""
    cfg.log.debug("Testing various aspects of 'ringroots list'")
    nothing: Final = all(not item for item in exist)

    cfg.log.debug("- trivial, output not captured")
    run_ringroots(cfg, cont, ["list"])

    exist_str: Final = ["yes" if item else "no" for item in exist]
    cfg.log.debug(
        "- check, parse the output; expected: %(exist_str)s",
        {"exist_str": ", ".join(exist_str)},
    )
    lines, err_lines = run_ringroots_capture(cfg, cont, ["list", "--check"])

    complained: Final = any("No chroots are defined" in line for line in err_lines)
    if nothing and not complained:
        sys.exit(
            f"list --check did not complain about no chroots: "
            f"output {lines!r} errors {err_lines!r}"
        )
    elif complained and not nothing:
        sys.exit(f"list --check complained about no chroots: output {lines!r} errors {err_lines!r}")

    if lines != [
        "\t".join([chroot.name, f"{chroot.distro}/{chroot.release}", expected])
        for chroot, expected in zip(CHROOTS, exist_str, strict=True)
    ]:
        sys.exit(f"list --check produced unexpected output: {lines!r}")

    cfg.log.debug("Checking the contents of /etc/os-release")
    for chroot in itertools.compress(CHROOTS, exist):
        test_os_release(cfg, cont, chroot)


def test_os_release(cfg: Config, cont: Container, chroot: ChrootDef) -> None:
    """Make sure the schroot environment has a sensible os-release file."""
    cfg.log.debug("- looking for the root directory of %(name)s", {"name": chroot.name})
    pfx: Final = f"checking /etc/os-release for {chroot.name}"
    lines, err_lines = cont.run_capture(["schroot", "--config", "-c", chroot.name])
    if err_lines:
        sys.exit(f"{pfx}: schroot --config: output {lines!r}, errors {err_lines!r}")
    dir_caps: Final = [
        fields[1]
        for fields in (line.split("=", maxsplit=1) for line in lines)
        if fields[0] == "directory"
    ]
    if len(dir_caps) != 1:
        sys.exit(f"{pfx}: scrhoot --config: not a single directory= line: {lines!r}")
    chroot_dir: Final = pathlib.Path(dir_caps[0])
    os_rel_file: Final = chroot_dir / "etc/os-release"

    cfg.log.debug("- reading %(os_rel_file)s", {"os_rel_file": os_rel_file})
    lines, err_lines = cont.run_capture(
        [
            "sh",
            "-c",
            f"unset ID VERSION_ID; . '{os_rel_file}'; echo \"i$ID v$VERSION_ID\"",
        ]
    )
    if err_lines:
        sys.exit(f"{pfx}: output {lines!r}, errors {err_lines!r}")
    if len(lines) != 1:
        sys.exit(f"{pfx}: not a single line: {lines!r}")

    ver_m: Final = RE_DISTRO_VERSION.match(lines[0])
    if not ver_m:
        sys.exit(f"{pfx}: weird output: {lines!r}")
    distro, version_id = ver_m.group("distro"), ver_m.group("version_id")
    cfg.log.debug(
        "- distro %(distro)r version_id %(version_id)r",
        {"distro": distro, "version_id": version_id},
    )
    if distro != chroot.distro or version_id != chroot.version_id:
        sys.exit(
            f"{pfx}: expected distro '{chroot.distro}' version_id "
            f"'{chroot.version_id}', got distro '{distro}' version_id '{version_id}'"
        )


def test_create(cfg: Config, cont: Container) -> None:
    """Test the creation of a single specified chroot environment."""
    cfg.log.debug("Testing the creation of a single chroot environment.")

    cfg.log.debug("- ought to be successful")
    run_ringroots(cfg, cont, ["create", CHROOTS[1].name])

    cfg.log.debug("- there should be only one")
    lines, err_lines = cont.run_capture(["schroot", "-l"])
    if lines != [f"chroot:{CHROOTS[1].name}", f"source:{CHROOTS[1].name}"] or err_lines:
        sys.exit(f"Unexpected schroot -l output: {lines!r} errors: {err_lines!r}")

    test_list(cfg, cont, [False, True] + ([False] * (len(CHROOTS) - 2)))

    cfg.log.debug("Now testing the creation of all the missing chroot environments")
    run_ringroots(cfg, cont, ["create", "--missing"])

    cfg.log.debug("- let's see them all")
    lines, err_lines = cont.run_capture(["schroot", "-l"])
    if (
        lines
        != sorted(
            [f"chroot:{chroot.name}" for chroot in CHROOTS]
            + [f"source:{chroot.name}" for chroot in CHROOTS]
        )
        or err_lines
    ):
        sys.exit(f"Unexpected schroot -l output: {lines!r} errors: {err_lines!r}")

    test_list(cfg, cont, [True] * len(CHROOTS))


def test_remove(cfg: Config, cont: Container) -> None:
    """Test the removal of a single specified chroot environment."""
    cfg.log.debug("Let us remove a couple of chroot environments")
    exist: Final = [True] * len(CHROOTS)
    order: Final = [item for item in [1, 2, 0] if item < len(CHROOTS)] + list(
        range(3, len(CHROOTS))
    )
    cfg.log.debug("- chroots order: %(order)s", {"order": order})
    for idx in order:
        if not exist[idx]:
            sys.exit(f"Expected exist[{idx}] to be true: {exist!r}")
        chroot = CHROOTS[idx]
        cfg.log.debug("- removing %(name)s", {"name": chroot.name})

        run_ringroots(cfg, cont, ["remove", "--", chroot.name])

        exist[idx] = False
        test_list(cfg, cont, exist)

    if any(exist):
        sys.exit(f"Expected exist to contain all false values: {exist!r}")


def run_test(cfg: Config) -> None:
    """Start a container, run the test."""
    with tempfile.TemporaryDirectory(dir="/tmp") as tempd_obj:
        tempd: Final = pathlib.Path(tempd_obj)
        cfg.log.debug("Using %(tempd)s as a temporary directory", {"tempd": tempd})

        homedir: Final = tempd / "home"
        homedir.mkdir(mode=0o755)

        with run_container(cfg, homedir=homedir) as cont_prep:
            cont_prep.check_call(["printenv"], as_root=True)
            cont_prep.check_call(["ls", "-l", "/opt/ringroots"], as_root=True)

            cont = prepare_container(cfg, cont_prep)

            test_list(cfg, cont, [False] * len(CHROOTS))

            test_create(cfg, cont)

            test_remove(cfg, cont)


@click.command(name="test_docker")
@click.option(
    "--image",
    "-i",
    type=str,
    required=True,
    help="the Docker image tag to use, e.g. 'bookworm'",
)
@click.option(
    "--python-version",
    "-p",
    type=str,
    default="3.11",
    help="the Python version to use (default: 3.11)",
)
@click.option("--verbose", "-v", is_flag=True, help="verbose operation; display diagnostic output")
def main(*, image: str, python_version: str, verbose: bool) -> None:
    """Parse command-line arguments, run the test."""
    cfg = Config(
        group_id=posix.getgid(),
        image=image,
        log=utils.build_logger(verbose=verbose),
        python_version=python_version,
        srcdir=pathlib.Path.cwd(),
        utf8_env=utf8_locale.UTF8Detect().detect().env,
        user_id=posix.getuid(),
    )
    run_test(cfg)


if __name__ == "__main__":
    main()
