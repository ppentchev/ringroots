# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Test the handling of schroot environments."""

from __future__ import annotations

import os
import pathlib
import posix
import tempfile
import typing

import pytest

from ringroots import defs
from ringroots import utils
from ringroots.chenv import schroot


if typing.TYPE_CHECKING:
    from typing import Final


def create_sudo_wrapper(sudo: pathlib.Path, logfile: pathlib.Path) -> None:
    """Create a wrapper that simulates sudo."""
    sudo.write_text(
        f"""#!/bin/sh

if [ "$1" = '--' ]; then
    shift
fi

printf -- '%s\n' "$*" >> '{logfile}'
"$@"
""",
        encoding="UTF-8",
    )
    sudo.chmod(0o755)


@pytest.mark.parametrize(
    ("lines", "expected", "log_count"),
    [
        (
            ["root-groups=root,sbuild"],
            ["root-groups=root,sbuild", "source-root-groups=root,sbuild"],
            1,
        ),
        (
            ["root-groups=nobody,sbuild"],
            [
                "root-groups=nobody,sbuild,root",
                "source-root-groups=nobody,sbuild,root",
            ],
            1,
        ),
        (
            ["root-groups=root,sbuild", "source-root-groups=nobody"],
            ["root-groups=root,sbuild", "source-root-groups=nobody,root,sbuild"],
            1,
        ),
        (
            ["root-groups=bin,daemon", "source-root-groups=root,nobody"],
            [
                "root-groups=bin,daemon,root,sbuild",
                "source-root-groups=root,nobody,bin,daemon,sbuild",
            ],
            1,
        ),
        (
            ["root-groups=bin,sbuild,nobody,root"],
            [
                "root-groups=bin,sbuild,nobody,root",
                "source-root-groups=bin,sbuild,nobody,root",
            ],
            1,
        ),
        (
            ["root-groups=root,sbuild", "source-root-groups=root,sbuild"],
            ["root-groups=root,sbuild", "source-root-groups=root,sbuild"],
            0,
        ),
    ],
)
def test_setup_source_root_groups(*, lines: list[str], expected: list[str], log_count: int) -> None:
    """Make sure the source-root-groups directive is present and correct."""
    posix.umask(0o022)
    with tempfile.TemporaryDirectory(prefix="test-schroot.") as tempd_obj:
        tempd: Final = pathlib.Path(tempd_obj)
        print(f"\nUsing {tempd} as a temporary directory")

        sudo: Final = tempd / "do"
        logfile: Final = tempd / "sudo.log"
        logfile.touch(mode=0o644)
        create_sudo_wrapper(sudo, logfile)

        cfg: Final = defs.Config(
            chroots=[],
            chroots_by_name={},
            log=utils.build_logger(verbose=True),
            keyrings={},
            mirrors={},
            config_file=pathlib.Path("/nonexistent"),
            sudo=str(sudo),
            utf8_env={},
            verbose=True,
        )

        tempf: Final = tempd / "whee-def"
        tempf.write_text(
            "".join(line + "\n" for line in ["[whee]", "name=whee", *lines]), encoding="UTF-8"
        )
        schroot.SChroot.setup_source_root_groups(cfg, "whee", tempf)

        output_lines: Final = [
            line for line in tempf.read_text(encoding="UTF-8").splitlines() if line
        ]
        assert output_lines == ["[whee]", "name=whee", *expected]

        log_lines: Final = logfile.read_text(encoding="UTF-8").splitlines()
        assert len(log_lines) == log_count
        for line in log_lines:
            assert line.startswith(f"install -o {os.getuid()} -g {os.getgid()} -m 644 -- ")
