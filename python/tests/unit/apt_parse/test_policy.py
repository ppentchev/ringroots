# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Test parsing the `apt-cache policy` output."""

from __future__ import annotations

import pathlib

import pytest

import ringroots.vendor.apt_parse.output as apoutput


@pytest.mark.parametrize(
    ("filename", "repos"),
    [
        (
            "policy-large.txt",
            [
                ("unstable", "main", "repo.storpool.com"),
                ("UNRELEASED", "main", "debian.ringlet.net"),
                ("lunar-proposed", "multiverse", "straylight.m.ringlet.net"),
                ("testing", "main", "127.0.0.1"),
            ],
        ),
        (
            "policy-small-localhost.txt",
            [("sid", "contrib", "127.0.0.1"), ("sid", "main", "127.0.0.1")],
        ),
        (
            "policy-bullseye.txt",
            [
                ("bullseye", "main", "deb.debian.org"),
                ("bullseye-security", "updates/main", "security.debian.org"),
            ],
        ),
    ],
)
def test_parse_policy_output(filename: str, repos: list[tuple[str, str, str]]) -> None:
    """Parse the saved output of `apt-cache policy`."""
    fname = pathlib.Path(__file__).with_name(filename)
    contents = fname.read_text(encoding="UTF-8")
    res = {
        (repo.suite, repo.component, repo.origin): repo
        for repo in apoutput.parse_policy_repos(contents)
    }
    for needle in repos:
        assert needle in res
