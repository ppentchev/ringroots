# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Run the `ringroots` tool, check its output and operation."""

from __future__ import annotations

import contextlib
import dataclasses
import json
import os
import pathlib
import subprocess  # noqa: S404
import sys
import tempfile
import typing

import pytest
import tomli_w


if typing.TYPE_CHECKING:
    from collections.abc import Iterator
    from typing import Any, Final


@dataclasses.dataclass(frozen=True)
class Environment:
    """Settings related to the temporary directory."""

    tempd: pathlib.Path
    """The temporary directory itself."""

    home: pathlib.Path
    """The home directory."""

    config_file: pathlib.Path
    """The path to the config file within the home directory."""

    config_text: str
    """The text to be written out to the configuration file."""

    commands_log: pathlib.Path
    """The path to the file where the wrappers will log their invocation command lines."""

    env: dict[str, str]
    """The environment to run child processes (e.g. `ringroots`) in."""


@contextlib.contextmanager
def temp_home() -> Iterator[Environment]:
    """Prepare a temporary directory, clean it up afterwards."""
    with tempfile.TemporaryDirectory() as tempd_obj:
        tempd: Final = pathlib.Path(tempd_obj)

        bindir: Final = tempd / "bin"
        bindir.mkdir(mode=0o755)

        wrapper_dir: Final = pathlib.Path(__file__).parent / "cmd"
        for cmd in (
            "mock",
            "schroot",
        ):
            wrapper_text = f"#!{sys.executable}\n" + (wrapper_dir / f"{cmd}.py").read_text(
                encoding="UTF-8"
            )
            # This will not really work on win32, will it...
            wrapper_path = bindir / cmd
            wrapper_path.write_text(wrapper_text, encoding="UTF-8")
            wrapper_path.chmod(0o755)

        home: Final = tempd / "home"
        home.mkdir(mode=0o755)

        config_file: Final = home / ".config/ringroots.toml"
        config_file.parent.mkdir(mode=0o755, parents=True)

        config_data: Final[dict[str, Any]] = {
            "format": {"version": {"major": 0, "minor": 2}},
            "chroots": {
                "ringroots-test-ubuntu": {"distro": "ubuntu", "release": "jammy"},
                "ringroots-test-deb": {"distro": "debian", "release": "bookworm"},
                "ringroots-test-c8-x86_64": {"distro": "centos", "release": "8"},
                "ringroots-test-c9-x86_64": {"distro": "centos", "release": "9"},
            },
        }
        config_text: Final = tomli_w.dumps(config_data)

        commands_log: Final = tempd / "commands.log"

        env: Final = dict(os.environ)
        env["HOME"] = str(home)
        env["PATH"] = ":".join((str(bindir), env["PATH"]))
        env["RINGROOTS_TEST_LOG"] = str(commands_log)

        yield Environment(
            tempd=tempd,
            home=home,
            config_file=config_file,
            config_text=config_text,
            commands_log=commands_log,
            env=env,
        )


def assert_commands(env: Environment, cmds: list[list[str]]) -> None:
    """Make sure the logged commands are as expected."""
    if not env.commands_log.is_file():
        assert not cmds
        return

    logged_cmds: Final[list[list[str]]] = [
        json.loads(line) for line in env.commands_log.read_text(encoding="UTF-8").splitlines()
    ]
    assert isinstance(logged_cmds, list), repr(logged_cmds)
    assert all(isinstance(cmd, list) for cmd in logged_cmds), repr(logged_cmds)
    assert all(all(isinstance(word, str) for word in cmd) for cmd in logged_cmds), repr(logged_cmds)

    for expected, logged in zip(cmds, logged_cmds, strict=True):
        assert logged[0] == str(env.tempd / "bin" / expected[0])
        assert logged[1 : len(expected)] == expected[1:]


def test_features() -> None:
    """Prepare a temporary directory, run `ringroots --features`, check its output."""
    with temp_home() as env:
        lines: Final = subprocess.check_output(
            ["ringroots", "--features"],
            encoding="UTF-8",
            env=env.env,
        ).splitlines()
        match lines:
            case [single]:
                assert single.startswith("Features: ")

            case other:
                raise AssertionError(repr(other))


def test_list_no_config() -> None:
    """Expect `ringroots list` to fail without a configuration file."""
    with temp_home() as env, pytest.raises(subprocess.CalledProcessError):
        subprocess.check_call(["ringroots", "list"], env=env.env)


def test_list() -> None:
    """Prepare a temporary directory, run `ringroots list`, check its output."""
    with temp_home() as env:
        env.config_file.write_text(env.config_text, encoding="UTF-8")
        lines: Final = subprocess.check_output(
            ["ringroots", "list"],
            encoding="UTF-8",
            env=env.env,
        ).splitlines()
        assert lines == [
            "ringroots-test-c8-x86_64\tcentos/8",
            "ringroots-test-c9-x86_64\tcentos/9",
            "ringroots-test-deb\tdebian/bookworm",
            "ringroots-test-ubuntu\tubuntu/jammy",
        ]
        assert_commands(env, [])


def test_list_check() -> None:
    """Prepare a temporary directory, run `ringroots list --check`, check its output."""
    with temp_home() as env:
        env.config_file.write_text(env.config_text, encoding="UTF-8")
        lines: Final = subprocess.check_output(
            ["ringroots", "list", "--check"],
            encoding="UTF-8",
            env=env.env,
        ).splitlines()
        assert lines == [
            "ringroots-test-c8-x86_64\tcentos/8\tno",
            "ringroots-test-c9-x86_64\tcentos/9\tyes",
            "ringroots-test-deb\tdebian/bookworm\tyes",
            "ringroots-test-ubuntu\tubuntu/jammy\tno",
        ]
        assert_commands(
            env,
            [
                ["schroot", "--config", "--all"],
                ["mock", "--root", "ringroots-test-c8-x86_64", "--print-root-path"],
                ["mock", "--root", "ringroots-test-c9-x86_64", "--print-root-path"],
            ],
        )
