# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""A fake `schroot` command that only accepts a couple of options."""

from __future__ import annotations

import json
import os
import pathlib
import sys
import typing


if typing.TYPE_CHECKING:
    from typing import Final


EXPECTED: Final = [
    (
        ["--root", "ringroots-test-c9-x86_64", "--print-root-path"],
        [
            "/var/lib/mock/ringroots-test-c9-x86-64-or-something-else",
        ],
    ),
]
"""The way we expect to be invoked."""


def main() -> None:
    """Make sure the command-line options are as expected."""
    with pathlib.Path(os.environ["RINGROOTS_TEST_LOG"]).open(mode="at") as cmdlog:
        print(json.dumps(sys.argv), file=cmdlog)

    for args, output in EXPECTED:
        if sys.argv[1:] == args:
            print("".join(line + "\n" for line in output), end="")
            return

    raise RuntimeError(repr(sys.argv))


if __name__ == "__main__":
    main()
