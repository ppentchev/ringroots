<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# ringroots - create, update, or remove Debian chroot environments

The `ringroots` utility is a simple wrapper around the `schroot` tool.
It reads a TOML configuration file (located at `~/.config/ringroots.toml`)
with the following structure:

    [format.version]
    major = 0
    minor = 1
    
    [[chroots]]
    name = "unstable-amd64"
    distro = "debian"
    release = "unstable"
    
    [[chroots]]
    name = "unstable-i386"
    distro = "debian"
    release = "unstable"
    
    [[chroots]]
    name = "bullseye-amd64"
    distro = "debian"
    release = "bullseye"

The `ringroots create --missing` command will create any chroot environments
defined in the configuration file that are not yet present on the system.

The `ringroots update --existing` command will update the packages in all
the configured chroot environments that have been created.

## Contact

The `ringroots` tool is developed in
[a GitLab repository](https://gitlab.com/ppentchev/ringroots).
It was written by [Peter Pentchev](mailto:roam@ringlet.net).
