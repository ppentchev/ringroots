<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# test-local - a test tool for the ringroots command-line utility

This is a test tool for `ringroots`.
Nothing much to see here.

## Contact

The `ringroots` tool is developed in
[a GitLab repository](https://gitlab.com/ppentchev/ringroots).
It was written by [Peter Pentchev](mailto:roam@ringlet.net).
