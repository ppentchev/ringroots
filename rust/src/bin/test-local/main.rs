//! test-local: run some ringroots tests on the host system.
//!
//! This tool creates and removes schroot environments directly on
//! the system it is run on, not in a Docker container or anything.
//! It assumes that `schroot` and `sudo` have been configured for
//! the account it is run from.
/*
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

#![deny(missing_docs)]

mod clean;
mod cli;
mod defs;
mod run;
mod util;

use anyhow::{Context, Result};

use cli::Mode;

fn main() -> Result<()> {
    match cli::parse_args().context("Could not parse the command-line arguments")? {
        Mode::Clean(cfg) => clean::clean_test_chroots(&cfg)?,
        Mode::Run(cfg, program) => {
            clean::clean_test_chroots(&cfg)?;
            run::run_tests(&cfg, &program)?;
            clean::clean_test_chroots(&cfg)?;
        }
    };
    Ok(())
}
