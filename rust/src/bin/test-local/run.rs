//! Run some tests on the ringroots program.
/*
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

use std::collections::HashMap;
use std::fs;
use std::str::FromStr;

use anyhow::{ensure, Context, Error as AnyError, Result};
use itertools::Itertools;
use once_cell::sync::Lazy;
use regex::{Error as RegexError, Regex};
use tracing::{debug, trace};

use crate::defs::{Config as MainConfig, FORMAT_VERSION, NAME_PREFIX};
use crate::util;

/// Runtime configuration for the tests.
#[derive(Debug)]
struct Config<'data> {
    program: &'data str,
    cfgname: &'data str,
    utf8_env: HashMap<String, String>,
}

const CONFIG_TEXT: &str = r#"
[format.version]
major = @FORMAT_MAJ@
minor = @FORMAT_MIN@

[chroots.@NAME_PREFIX@bullseye-amd64]
distro = "debian"
release = "bullseye"

[chroots.@NAME_PREFIX@ringlet-unstable-amd64]
distro = "debian"
release = "unstable"

[chroots.@NAME_PREFIX@ringlet-unstable-amd64.repos.ringlet]
source = "https://debian.ringlet.net/debian-ringlet/"
suites = ["UNRELEASED", "unstable"]
architectures = ["amd64"]
keyring = "ringlet"

[chroots.@NAME_PREFIX@bionic-dh-12-amd64]
distro = "ubuntu"
release = "bionic"

[chroots.@NAME_PREFIX@bionic-dh-12-amd64.repos.backports]
flavor = "backports"
components = ["main", "universe"]
packages = ["debhelper", "dwz"]

[chroots.@NAME_PREFIX@focal-c-deadsnakes-amd64]
distro = "ubuntu"
release = "focal"

[chroots.@NAME_PREFIX@focal-c-deadsnakes-amd64.repos.cloud]
flavor = "add-apt-repo"
source = "ppa:deadsnakes"
packages = ["python3.11"]

[keyrings.ringlet]
url = "https://debian.ringlet.net/keys/ringlet-keyring.gpg"

[keyrings.ringlet.checksums]
sha256 = "f01dd6a0511e1e3addc73c8bd73bc17835290abe6fba922ab025a21de4a60827"
sha512 = "c2d877ed836d40f4e74cbc7b4a355fbc444026558e9e04279a09bfa1ef721c14e8d19dce36bdad336156db59fa550e9ef5d9144a91543c30a94334f7c15a5ebf"
"#;

type ExpectedTuple<'exp> = (&'exp str, &'exp str, &'exp str);
type ExpectedVec<'exp> = Vec<ExpectedTuple<'exp>>;
type ExpectedSlice<'exp> = &'exp [ExpectedTuple<'exp>];

/// Make sure `ringroots list --check` outputs the correct values.
fn test_list<'data>(cfg: &'data Config<'data>, expected: ExpectedSlice<'_>) -> Result<()> {
    for (read, (exp_name, exp_release, exp_present)) in util::check_output(
        cfg.program,
        &["-f", cfg.cfgname, "list", "--check"],
        &cfg.utf8_env,
    )
    .context("Could not run `ringroots --check`")?
    .lines()
    .map(|line| line.split('\t').collect::<Vec<_>>())
    .zip_eq(
        expected
            .iter()
            .map(|&(name, release, present)| (format!("{NAME_PREFIX}{name}"), release, present))
            .sorted(),
    ) {
        let okay = match &*read {
            &[name, release, present] => {
                (name, release, present) == (&exp_name, exp_release, exp_present)
            }
            _ => false,
        };
        ensure!(okay, format!(
                "Unexpected schroot --list --check output line: expected ({exp_name:?}, {exp_release:?}, {exp_present:?}), got {read:?}",
            ));
    }
    debug!(
        "Looks like we got {count} matching chroot{plu}",
        count = expected.len(),
        plu = if expected.len() == 1 { "" } else { "s" }
    );
    Ok(())
}

fn test_create<'data, 'exp>(
    cfg: &'data Config<'data>,
    name: &str,
    current: ExpectedVec<'exp>,
) -> Result<ExpectedVec<'exp>> {
    let chroot = format!("{NAME_PREFIX}{name}");
    debug!("About to create the {chroot} chroot environment");

    let expected: ExpectedVec<'exp> = {
        let (matched, res): (Vec<bool>, Vec<ExpectedTuple<'exp>>) = current
            .into_iter()
            .map(
                |(c_name, c_release, c_present)| -> Result<(bool, ExpectedTuple<'exp>)> {
                    if c_name == name {
                        ensure!(
                            c_present != "yes",
                            format!(
                            "Internal error: test_create() invoked, '{name}' already marked as present",
                        )
                        );
                        Ok((true, (c_name, c_release, "yes")))
                    } else {
                        Ok((false, (c_name, c_release, c_present)))
                    }
                },
            )
            .collect::<Result<Vec<_>, _>>()?
            .into_iter()
            .unzip();

        let c_matched = matched.into_iter().filter(|&flag| flag).map(|_| ()).count();
        ensure!(
            c_matched == 1,
            format!(
                "Internal error: test_create() invoked, '{name}' found {c_matched} times instead of 1"
            )
        );
        res
    };

    util::check_call(
        cfg.program,
        &["-f", cfg.cfgname, "create", &chroot],
        &cfg.utf8_env,
    )
    .with_context(|| {
        format!(
            "Could not create the {chroot} chroot",
            chroot = chroot.escape_debug()
        )
    })?;

    test_list(cfg, &expected)?;
    Ok(expected)
}

fn test_ringlet_unstable<'data, 'exp>(
    cfg: &'data Config<'data>,
    current: ExpectedVec<'exp>,
) -> Result<ExpectedVec<'exp>> {
    debug!("Creating the ringlet-unstable chroot");
    let expected = test_create(cfg, "ringlet-unstable-amd64", current)?;

    debug!("Making sure the ringlet-unstable chroot knows about sssnips");
    let policy = util::check_output(
        "schroot",
        &[
            "-c",
            &format!("{NAME_PREFIX}ringlet-unstable-amd64"),
            "-u",
            "root",
            "-d",
            "/",
            "--",
            "env",
            "LC_ALL=C.UTF-8",
            "LANGUAGE=",
            "apt-cache",
            "policy",
            "sssnips",
        ],
        &cfg.utf8_env,
    )
    .context("Could not run `apt-cache policy` within the ringlet-unstable schroot")?;
    debug!("Got sssnips policy data:\n{policy}");
    for needle in [
        "Installed: (none)",
        "Candidate: 0.",
        "/debian-ringlet unstable/main",
    ] {
        ensure!(
            policy.contains(needle),
            format!("No {needle:?} in {policy:?}")
        );
    }

    Ok(expected)
}

fn check_installed_version<'data>(
    cfg: &'data Config<'data>,
    chroot: &str,
    package: &str,
    version: u32,
) -> Result<()> {
    static RE_VERSION: Lazy<Result<Regex, RegexError>> =
        Lazy::new(|| Regex::new("(?x) ^ (?P<major> 0 | [1-9][0-9]* ) [.~-]"));

    debug!(
        "Making sure the {chroot} chroot has the correct {package} version - at least {version}"
    );
    let line = util::check_output(
        "schroot",
        &[
            "-c",
            &format!("{NAME_PREFIX}{chroot}"),
            "-u",
            "root",
            "-d",
            "/",
            "--",
            "dpkg-query",
            "-W",
            "-f",
            "${Version}",
            "--",
            package,
        ],
        &cfg.utf8_env,
    )
    .with_context(|| {
        format!("Could not query for the {package} package in the {chroot} schroot")
    })?;
    debug!("Got {package} version string {line}");

    let caps = RE_VERSION
        .as_ref()
        .map_err(|err| {
            AnyError::new(err).context("Internal error: could not compile run::RE_VERSION")
        })?
        .captures(&line)
        .with_context(|| format!("Could not parse the {package} version {line}"))?;
    let int_v = u32::from_str(
        caps.name("major")
            .with_context(|| format!("Internal error: no 'major' in {caps:?}"))?
            .as_str(),
    )
    .with_context(|| {
        format!(
            "Internal error: could not parse the 'major' part of {caps:?} as an unsigned integer"
        )
    })?;
    ensure!(
        int_v >= version,
        format!("Wrong {package} version {line} - major version {int_v} less than {version}")
    );
    Ok(())
}

fn test_focal_deadsnakes_python<'data, 'exp>(
    cfg: &'data Config<'data>,
    current: ExpectedVec<'exp>,
) -> Result<ExpectedVec<'exp>> {
    debug!("Creating the focal-deadsnakes chroot");
    let expected = test_create(cfg, "focal-c-deadsnakes-amd64", current)
        .context("Could not create the focal-deadsnakes chroot")?;
    check_installed_version(cfg, "focal-c-deadsnakes-amd64", "python3.11", 3)?;
    Ok(expected)
}

fn test_bionic_backports<'data, 'exp>(
    cfg: &'data Config<'data>,
    current: ExpectedVec<'exp>,
) -> Result<ExpectedVec<'exp>> {
    debug!("Creating the bionic-backports chroot");
    let expected = test_create(cfg, "bionic-dh-12-amd64", current)
        .context("Could not create the bionic-backports chroot")?;
    check_installed_version(cfg, "bionic-dh-12-amd64", "debhelper", 12)?;
    Ok(expected)
}

fn test_remaining<'data, 'exp>(
    cfg: &'data Config<'data>,
    current: ExpectedVec<'exp>,
) -> Result<ExpectedVec<'exp>> {
    util::check_call(
        cfg.program,
        &["-f", cfg.cfgname, "create", "--missing"],
        &cfg.utf8_env,
    )
    .context("Could not create the remaining chroots")?;

    ensure!(
        current.iter().any(|&(_, _, present)| present != "yes"),
        format!("Internal error: test_remaining() invoked with current: {current:?}")
    );
    let expected: ExpectedVec<'exp> = current
        .into_iter()
        .map(|(name, release, _)| (name, release, "yes"))
        .collect();

    debug!("Making sure all the chroot environments exist now");
    test_list(cfg, &expected)?;
    Ok(expected)
}

/// Run some actual tests.
pub fn run_tests(main_cfg: &MainConfig, program: &str) -> Result<()> {
    debug!("Running some tests");
    let tempd = tempfile::tempdir().context("Could not create a temporary directory")?;
    debug!(
        "Using {tempd} as a temporary directory",
        tempd = tempd.path().display()
    );

    let cfgfile = tempd.path().join("ringroots.toml");
    fs::write(
        &cfgfile,
        CONFIG_TEXT
            .replace("@NAME_PREFIX@", NAME_PREFIX)
            .replace(
                "@FORMAT_MAJ@",
                &format!("{major}", major = FORMAT_VERSION.0),
            )
            .replace(
                "@FORMAT_MIN@",
                &format!("{minor}", minor = FORMAT_VERSION.1),
            ),
    )
    .with_context(|| {
        format!(
            "Could not write out the temporary config file {cfgfile}",
            cfgfile = cfgfile.display()
        )
    })?;
    if main_cfg.verbose {
        let contents = fs::read_to_string(&cfgfile).with_context(|| {
            format!(
                "Could not read the contents of {cfgfile} back",
                cfgfile = cfgfile.display()
            )
        })?;
        trace!(contents);
    }
    let cfgname = cfgfile.to_str().with_context(|| {
        format!(
            "Could not build a string representation of {cfgfile}",
            cfgfile = cfgfile.display()
        )
    })?;
    let cfg = Config {
        program,
        cfgname,
        utf8_env: main_cfg.utf8_env.clone(),
    };

    debug!("Making sure none of the chroots exist at the start");
    let exp_start = vec![
        ("bullseye-amd64", "debian/bullseye", "no"),
        ("ringlet-unstable-amd64", "debian/unstable", "no"),
        ("bionic-dh-12-amd64", "ubuntu/bionic", "no"),
        ("focal-c-deadsnakes-amd64", "ubuntu/focal", "no"),
    ];
    test_list(&cfg, &exp_start)?;

    let exp_end = [
        test_focal_deadsnakes_python,
        test_ringlet_unstable,
        test_bionic_backports,
        test_remaining,
    ]
    .into_iter()
    .try_fold(exp_start.clone(), |current, func| func(&cfg, current))?;
    for ((st_name, st_release, _), actual) in exp_start.into_iter().zip_eq(exp_end.into_iter()) {
        let expected = (st_name, st_release, "yes");
        ensure!(
            actual == expected,
            format!("Unexpected state at the end: expected {expected:?}, got {actual:?}")
        );
    }
    Ok(())
}
