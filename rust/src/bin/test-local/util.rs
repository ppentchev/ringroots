//! Utility functions for the host test suite.
/*
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

use std::collections::HashMap;
use std::process::Command;

use anyhow::{ensure, Context, Result};

pub fn check_call(command: &str, args: &[&str], utf8_env: &HashMap<String, String>) -> Result<()> {
    let slug = format!("`{} {}`", command, args.join(" "));
    ensure!(
        Command::new(command)
            .args(args)
            .envs(utf8_env)
            .status()
            .with_context(|| format!("Could not run {slug}"))?
            .success(),
        format!("{slug} failed")
    );
    Ok(())
}

pub fn check_output(
    command: &str,
    args: &[&str],
    utf8_env: &HashMap<String, String>,
) -> Result<String> {
    let slug = format!("`{cmd} {args}`", cmd = command, args = args.join(" "));
    let output = Command::new(command)
        .args(args)
        .envs(utf8_env)
        .output()
        .with_context(|| format!("Could not run {slug}"))?;
    ensure!(
        output.status.success(),
        format!("Could not run {slug}: {output:?}")
    );
    String::from_utf8(output.stdout)
        .with_context(|| format!("Could not parse the output of {slug}"))
}
