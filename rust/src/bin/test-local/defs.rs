//! Common definitions for the test-local tool.
/*
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

use std::collections::HashMap;

/// Runtime configuration for the test-local tool.
#[derive(Debug)]
pub struct Config {
    /// The environment variables for a child process to produce UTF-8 output.
    pub utf8_env: HashMap<String, String>,

    /// Verbose operation: display diagnostic messages.
    pub verbose: bool,
}

pub const FORMAT_VERSION: (u32, u32) = (0, 2);

pub const NAME_PREFIX: &str = "ringroots-test-local-";
