//! Remove leftover environments created by the test-local tool.
/*
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

use std::collections::HashSet;
use std::fs::{self, DirEntry};
use std::path::{Path, PathBuf};
use std::process::Command;

use anyhow::{ensure, Context, Result};
use itertools::Itertools;
use tini::Ini;
use tracing::debug;

use crate::defs::{Config, NAME_PREFIX};
use crate::util;

const PATH_CHROOT_CONF: &str = "/etc/schroot/chroot.d";

/// A running schroot session.
#[derive(Debug)]
struct SessInfo {
    /// The name of the running session.
    name: String,

    /// The name of the chroot environment that the session was started for.
    original_name: String,
}

/// Get all the chroot sessions
fn get_all_sessions(cfg: &Config) -> Result<Vec<SessInfo>> {
    let slug = "`schroot --config --all-sessions`";
    Ini::from_string(
        util::check_output("schroot", &["--config", "--all-sessions"], &cfg.utf8_env)
            .context("Could not query schroot about the active sessions")?,
    )
    .with_context(|| format!("Could not parse the output of {slug}"))?
    .iter()
    .map(|(name, section)| -> Result<_> {
        let original_name = section
            .get_raw("original-name")
            .with_context(|| format!("No 'original-name' in [{name}] in the output of {slug}"))?;
        Ok(SessInfo {
            name: name.clone(),
            original_name: original_name.clone(),
        })
    })
    .collect::<Result<_, _>>()
}

/// Remove the running schroot sessions for a test environment.
#[allow(clippy::print_stderr)]
fn remove_sessions(cfg: &Config, name: &str) -> Result<()> {
    debug!("- looking for running sessions for {name}");
    let to_check: HashSet<_> = get_all_sessions(cfg)
        .context("Could not look for schroot sessions to remove")?
        .into_iter()
        .filter_map(|sess| {
            (sess.original_name == name).then(|| -> Result<_> {
                debug!("- trying to clean up the {name} session", name = sess.name);
                let slug = format!("`schroot -e -c {name}`", name = sess.name);
                if !Command::new("schroot")
                    .args(["-e", "-c", &sess.name])
                    .envs(&cfg.utf8_env)
                    .status()
                    .with_context(|| format!("Could not run {slug}"))?
                    .success()
                {
                    eprintln!("{slug} failed, carrying on");
                }
                Ok(sess.name)
            })
        })
        .collect::<Result<_, _>>()?;
    if !to_check.is_empty() {
        debug!(
            "- checking whether we managed to remove {count} session{plu}",
            count = to_check.len(),
            plu = if to_check.len() == 1 { "" } else { "s" }
        );
        let current: HashSet<_> = get_all_sessions(cfg)
            .context("Could not check whether we managed to remove some schroot sessions")?
            .into_iter()
            .map(|sess| sess.name)
            .collect();
        let mut not_removed: Vec<_> = current.intersection(&to_check).collect();
        not_removed.sort_unstable();
        ensure!(
            not_removed.is_empty(),
            format!(
                "Could not remove some chroot sessions: {not_removed}",
                not_removed = not_removed.into_iter().join(", ")
            )
        );
    }
    Ok(())
}

/// Remove a test environment's root directory and definition file.
fn do_remove(cfg: &Config, cfile: &Path, path: &str) -> Result<()> {
    debug!("- removing the {path} directory");
    util::check_call("sudo", &["--", "rm", "-rf", "--", path], &cfg.utf8_env)
        .with_context(|| format!("Could not remove the {path} directory"))?;
    ensure!(
        !Path::new(path).exists(),
        format!("The {path} directory still exists even after `sudo rm -rf`",)
    );
    debug!("- removing the {cfile} file", cfile = cfile.display());
    util::check_call(
        "sudo",
        &[
            "--",
            "rm",
            "--",
            &format!("{cfile}", cfile = cfile.display()),
        ],
        &cfg.utf8_env,
    )
    .with_context(|| format!("Could not remove the {path} file"))?;
    Ok(())
}

/// Find and remove leftover chroots.
pub fn clean_test_chroots(cfg: &Config) -> Result<()> {
    debug!("Looking for chroot environments to clean up");
    for cfile_res in fs::read_dir(PATH_CHROOT_CONF)
        .with_context(|| format!("Could not read the {PATH_CHROOT_CONF} directory"))?
        .map(|entry_res| -> Result<DirEntry> {
            entry_res.with_context(|| {
                format!("Could not read an entry from the {PATH_CHROOT_CONF} directory")
            })
        })
        .map_ok(|entry: DirEntry| -> Result<Option<PathBuf>> {
            Ok(entry
                .metadata()
                .with_context(|| {
                    format!("Could not examine {path}", path = entry.path().display())
                })?
                .is_file()
                .then(|| entry.path()))
        })
        .flatten_ok()
        .flatten_ok()
    {
        let cfile = cfile_res?;
        let desc = Ini::from_file(&cfile)
            .with_context(|| format!("Could not parse {}", cfile.display()))?;
        for (name, section) in desc
            .iter()
            .filter(|&(name, _)| name.starts_with(NAME_PREFIX))
        {
            let ctype = section
                .get_raw("type")
                .with_context(|| format!("No 'type' in [{}] in {}", name, cfile.display()))?;
            ensure!(
                ctype == "directory",
                format!(
                    "Expected type=directory in [{}] in {}, got '{}'",
                    name,
                    cfile.display(),
                    ctype,
                )
            );
            let path = section
                .get_raw("directory")
                .with_context(|| format!("No 'directory' in [{}] in {}", name, cfile.display(),))?;
            remove_sessions(cfg, name)?;
            do_remove(cfg, &cfile, path)?;
        }
    }
    Ok(())
}
