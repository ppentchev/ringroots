//! Command-line argument parsing for the test-local tool.
//!
//! Define the `clap` declarative command-line hierarchy.
/*
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

use std::path::Path;

use anyhow::{bail, ensure, Context, Result};
use clap::{Parser, Subcommand};
use feature_check::defs::{Config as FCConfig, Obtained as FCObtained};
use feature_check::obtain as fcobtain;
use feature_check::version::Version as FCVersion;
use std::io;
use tracing::Level;
use utf8_locale::Utf8Detect;

use crate::defs::{Config, FORMAT_VERSION};

#[derive(Debug, Subcommand)]
enum Command {
    /// Clean up leftover test chroot environments.
    Clean,

    /// Run the tests.
    Run {
        /// The (possibly relative) path to the ringroots program to test.
        program: String,
    },
}

#[derive(Debug, Parser)]
#[clap(author, version, about)]
struct Cli {
    /// Verbose operation: display diagnostic messages.
    #[clap(short)]
    verbose: bool,

    /// What to do, what to do?
    #[clap(subcommand)]
    command: Command,
}

/// What purpose was this program invoked for?
#[derive(Debug)]
pub enum Mode {
    /// Remove leftover test chroot environments.
    Clean(Config),

    /// Run the tests.
    Run(Config, String),
}

/// Make sure the program supports what we will try to test.
fn check_program_features(program: &str) -> Result<()> {
    let fccfg = FCConfig::default().with_program(program.to_owned());
    match fcobtain::obtain_features(&fccfg)
        .with_context(|| format!("Could not check `{program} --features`"))?
    {
        FCObtained::Failed(err) => {
            bail!(format!("Could not check `{program} --features`: {err}"))
        }
        FCObtained::NotSupported => bail!(format!(
            "The {program} program does not seem to support the --features option"
        )),
        FCObtained::Features(res) => {
            let prog_version = res
                .get("ringroots")
                .with_context(|| format!("No 'ringroots' in the `{program} --features` output"))?;
            ensure!(
                prog_version.as_ref().starts_with("0.2."),
                format!("Expected a 0.2.x program version, got {prog_version}")
            );

            let format_min = res
                .get("format-min")
                .with_context(|| format!("No 'format-min' in the `{program} --features` output"))?;
            let vmin: FCVersion = format_min.as_ref().parse().with_context(|| {
                format!("Invalid 'format-min' in the `{program} --features` output: {format_min}")
            })?;
            let format_max = res
                .get("format-max")
                .with_context(|| format!("No 'format-max' in the `{program} --features` output"))?;
            let vmax: FCVersion = format_max.as_ref().parse().with_context(|| {
                format!("Invalid 'format-max' in the `{program} --features` output: {format_max}")
            })?;
            let vours: FCVersion = format!(
                "{major}.{minor}",
                major = FORMAT_VERSION.0,
                minor = FORMAT_VERSION.1
            )
            .parse()
            .with_context(|| {
                format!(
                    "Internal error: could not parse {major}.{minor} as a version",
                    major = FORMAT_VERSION.0,
                    minor = FORMAT_VERSION.1
                )
            })?;
            ensure!(
                (vours >= vmin) && (vours <= vmax),
                format!(
                    "`{program} --features` reported format versions {vmin} to {vmax}, ours {vours} out of range"
                )
            );
        }
        other => bail!(format!(
            "Internal error: feature-check result {other:?} for {program:?} not handled"
        )),
    }
    Ok(())
}

/// Initialize the logging subsystem provided by the `tracing` library.
fn setup_tracing(verbose: bool) -> Result<()> {
    let sub = tracing_subscriber::fmt()
        .without_time()
        .compact()
        .with_max_level(if verbose { Level::TRACE } else { Level::INFO })
        .with_writer(io::stderr)
        .finish();
    #[allow(clippy::absolute_paths)]
    tracing::subscriber::set_global_default(sub).context("Could not initialize the tracing logger")
}

/// Parse the command-line arguments, exit on error.
pub fn parse_args() -> Result<Mode> {
    let args = Cli::parse();

    let cfg = Config {
        utf8_env: Utf8Detect::new()
            .detect()
            .context("Could not detect a UTF-8-compatible locale")?
            .env_vars,
        verbose: args.verbose,
    };
    setup_tracing(cfg.verbose).context("Could not initialize the logging subsystem")?;
    Ok(match args.command {
        Command::Clean => Mode::Clean(cfg),
        Command::Run { program } => {
            check_program_features(&program)?;
            Mode::Run(
                cfg,
                Path::new(&program)
                    .canonicalize()
                    .with_context(|| format!("Could not get the full path to {program}"))?
                    .to_str()
                    .with_context(|| format!("Could not represent the full path to {program}"))?
                    .to_owned(),
            )
        }
    })
}
