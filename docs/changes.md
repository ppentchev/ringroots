<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# Changelog

All notable changes to the ringroots project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.0] - 2024-01-14

### Incompatible changes

- Configuration:
    - only parse version `0.2` of the configuration file format
    - ignore any `tool.*` sections so that third-party tools can use
      the same configuration file

### Fixes

- Functionality:
    - make the Apt mirror configurable
- Python source:
    - rename the `Chroot.list()` method to `.list_all()` to avoid conflicts
- Testing:
    - do not run `ringroots create --missing` in the Tox test; most systems where
      this will be run do not really allow root access
    - do not pass `--python-version` to `mypy`, use the currently-running
      interpreter's version; we have other ways (Nix expressions) to test
      with different Python versions now

### Additions

- Switch to yearless SPDX copyright and license tags
- Configuration:
    - `chroot.*.repos`: Apt repositories to add, with the `url`, `backports`, and
      `add-apt-repo` flavors
    - `keyrings.*`: keyrings to install into `/usr/share/keyrings`
    - `mirrors.debian`, `mirrors.ubuntu`: Apt mirrors to use
- Functionality:
    - add Apt package repositories and install packages as specified in
      the configuration
    - add the `--features` command-line option, specify version strings for
      various aspects of the `ringroots` tool's operation
- Python source:
    - add `schroot.SChroot` methods to run commands within the chroot environment
    - add a vendored copy of the `apt-parse` library to parse the output of
      the `apt-cache policy` command, since the Python `apt` module may not
      always be installed in the chroot environment
    - add a vendored copy of the `logging-std` library to initialize
      the logging subsystem
- Testing:
    - add a simple Docker test suite
    - add a Rust test tool that makes sure that the chroot environments can
      be entered and commands can be run within them
    - tag the Tox environments so that the `tox-stages` tool may be used
    - add the `reuse` Tox environment for the SPDX tags
    - add Nix expressions for running the Python tests using Tox
    - import the `vetox` test tool for more Nix testing with different
      Python versions

### Other changes

- Functionality:
    - decide whether to use the merged-usr layout depending on
      the Debian/Ubuntu release within the chroot environment
    - use `sudo` for the `create` and `remove` actions if necessary
    - use the `typedload` library for parsing the configuration file
    - use `sbuild-createchroot` instead of Ubuntu's `mk-sbuild` for
      simpler, leaner chroot environment creation
- Python build system
    - switch from `setuptools` to `hatchling`
    - break the lists of library dependencies out into separate files
    - specify upper versions of the library dependencies
- Python source:
    - use `tomllib` instead of `tomli` on Python 3.11 and later;
      add `tomli` to the list of required packages for Python < 3.11
    - move the configuration of various build and linter tools to
      the `pyproject.toml` file
    - use `dict`, `list`, `tuple`, `type` instead of the `typing` generics
    - really use `... | None` instead of `Optional`
    - simulate the behavior of `StrEnum` for some types
    - use `utf8-locale` 1.x with no changes
    - move the Python modules to `python/src/` and the unit test suite to
      `python/tests/unit/` to catch installation issues during testing
    - switch from `cfg-diag` to the vendored `logging-std` library
    - add some more docstrings
    - move the list of features into the `ringroots.defs` module
    - use `click` instead of `argparse` for command-line argument parsing
    - add more `Final` qualifiers to variable initializations
- Testing:
    - convert the `tox.ini` file to the 4.x format
    - use `mypy` 1.7 or higher with no changes
    - rename the source formatting environments to `format` and `reformat`
    - switch from `black`, `flake8`, and `pylint` to `ruff`
    - minor fixes and refactoring as suggested by Ruff

## [0.1.0] - 2022-06-09

### Started

- First public release.

[Unreleased]: https://gitlab.com/ppentchev/ringroots/-/compare/release%2F0.2.0...main
[0.2.0]: https://gitlab.com/ppentchev/ringroots/-/compare/release%2F0.1.0...release%2F0.2.0
[0.1.0]: https://gitlab.com/ppentchev/ringroots/-/tags/release%2F0.1.0
