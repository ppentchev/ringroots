<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# ringroots - create, update, or remove Debian chroot environments

\[[Home][ringlet-home] | [GitLab][gitlab] | [ReadTheDocs][readthedocs]\]

The `ringroots` utility is a simple wrapper around the `schroot` tool.
It reads a TOML configuration file (located at `~/.config/ringroots.toml`)
with the following structure:

``` toml
[format.version]
major = 0
minor = 1

[[chroots]]
name = "unstable-amd64"
distro = "debian"
release = "unstable"

[[chroots]]
name = "unstable-i386"
distro = "debian"
release = "unstable"

[[chroots]]
name = "bullseye-amd64"
distro = "debian"
release = "bullseye"
```

The `ringroots create --missing` command will create any chroot environments
defined in the configuration file that are not yet present on the system.

The `ringroots update --existing` command will update the packages in all
the configured chroot environments that have been created.

## Contact

The `ringroots` tool was written by [Peter Pentchev][roam].
It is developed in [a GitLab repository][gitlab]. This documentation is
hosted at [Ringlet][ringlet-home] with a copy at [ReadTheDocs][readthedocs].

[roam]: mailto:roam@ringlet.net "Peter Pentchev"
[gitlab]: https://gitlab.com/ppentchev/ringroots "The ringroots GitLab repository"
[readthedocs]: https://ringroots.readthedocs.io/ "The ringroots ReadTheDocs page"
[ringlet-home]: https://devel.ringlet.net/sysutils/ringroots/ "The Ringlet ringroots homepage"
